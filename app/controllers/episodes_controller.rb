class EpisodesController < ApplicationController

	def show

	    season = params[:season].sub( /\D/, '' ).to_i( 10 )
	    number = params[:number].sub( /\D/, '' ).to_i( 10 )

		@tv_show = TvShow.find_by_slug( params[:tv_show_slug] )

	    # Try to find TvShow splitting slug with year
		if @tv_show.blank? then
			slug_and_year = []
			match = params[:tv_show_slug].match /^(.+)[-_](\d{4})$/

			if match

				slug_and_year << match[1]
				slug_and_year << match[2]

			end

			@tv_show = TvShow.find_by_slug( slug_and_year[0] ) if slug_and_year.length == 2

			raise ActiveRecord::RecordNotFound if @tv_show.blank?
		end

	    @episode = @previous = @next = nil

	    # Get episode requested plus previous and next. Hopefully it's ok to fetch all episodes from Tv Show.
	    @tv_show.episodes.each do |episode|

	    	if !@episode.nil? then

	    		@previous = episode
	    		break

	    	end

	    	if episode.season == season and episode.number == number then

	    		@episode = episode
	    		next

	    	end

	    	@next = episode

	    end

	    # Episode not found!
	    raise ActiveRecord::RecordNotFound if @episode.nil?

	    # is it effective with heavy caching?
		# impressionist @tv_show
		# impressionist @episode

	    # desperate measure
	    @tv_show.delay.fetch_external_data if ( @episode.name.blank? || @episode.description.blank? ) && @tv_show.can_fetch_external_data?

	    # fetch SOME backdrop
	    @backdrop = nil
	    if !@episode.thumb.blank?

	    	@backdrop = @episode.thumb_path( '770x370' )

	    else

	    	tv_show_backdrop = @tv_show.medias.where( :media_type => 'backdrop' ).order( 'rand()' ).first()
	    	@backdrop = tv_show_backdrop.media_path( '770x370' ) unless tv_show_backdrop.blank?

	    end

	    @canonical = url_for :action => :show


		respond_to do |format|
			format.html
			format.json { render json: @episode }
		end
	end

	def thumb

		season = params[:season].sub( /\D/, '' ).to_i( 10 )
		number = params[:number].sub( /\D/, '' ).to_i( 10 )

		tv_show = TvShow.find_by_slug!( params[:tv_show_slug].chop )
		episode = tv_show.episodes.find_by_season_and_number!( season, number )

		raise ActiveRecord::RecordNotFound if episode.thumb.blank?

		content = episode.build_thumb params[:dimension]

		render :text => content and return if !content.blank?

		redirect_to( episode.thumb_path( params[:dimension] ), :status => :moved_permanently )

	end

	def fetch_subtitles

		@episode = Episode.find_by_imdb_id( params[:id] )

		#@episode.fetch_subtitles

		respond_to do |format|

			format.html { render layout: false }
			format.json { render json: @episode.releases }

		end

	end

end
