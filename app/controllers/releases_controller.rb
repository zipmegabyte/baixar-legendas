class ReleasesController < ApplicationController

  def show
  
  	if params[:movie_slug]
  	
  		@production = Movie
  			.eager_load( {:releases => :subtitle}, :medias )
  			.where( :slug => params[:movie_slug] )
  			.first
  	
  	elsif params[:tv_show_slug]
  	
  		season = params[:season].sub( /\D/, '' ).to_i( 10 )
	    number = params[:number].sub( /\D/, '' ).to_i( 10 )

  		@production = Episode
			.eager_load( {:releases => :subtitle}, :tv_show )
			.joins( :tv_show )
			.where(
				tv_show: {
					:slug => params[:tv_show_slug]
				}, 
				:season => season, 
				:number => number )
			.first
  		
  	end
  	
  	raise ActiveRecord::RecordNotFound if @production.blank?

  	release = @production.releases.to_a.find {|release| release.name.downcase == params[:release].downcase}
  	
  	raise ActiveRecord::RecordNotFound if release.blank?
  	
  	# Avoiding extra queries
  	release.owner = @production
  	@production.releases.each {|release| release.owner = @production}
  	@production.medias.each {|media| media.owner = @production} if @production.class.name == 'Movie'
  	@subtitle = release.subtitle
  	
  	raise ActiveRecord::RecordNotFound if @subtitle.blank?
  	
  	@subtitle.release = release

  	@download_count = 0
  	
  	result = ActiveRecord::Base.connection.select_all "SELECT COUNT(*) total FROM impression WHERE impressionable_type = 'Subtitle' AND impressionable_id = #{@subtitle.id}"
  	
  	result.each {|r| @download_count = r['total']}
  	  	
  	@isMovie = @subtitle.release.owner.class.name == 'Movie'
  	
  	subtitle_text = SRT::File.parse( @subtitle.text );
  	@subtitle_lines = subtitle_text.lines
  	
  	@canonical = url_for :action => :show
  	
  end
  
end
