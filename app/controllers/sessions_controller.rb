class SessionsController < ApplicationController

	def facebook_login

		response = {'success' => false}

		# Do not continue if there is no token being sent
		if !params[:token]

			destroy_session

			response[:message] = 'No token provided.'
			render json: response
			return

		end

		# Try to get user data from Facebook
		res = Net::HTTP.get_response( URI( "https://graph.facebook.com/me?access_token=#{URI.escape params[:token]}" ) )

		# Stop if can't connect to Facebook
		if !res.is_a?( Net::HTTPSuccess )

			destroy_session

			if res.is_a?( Net::HTTPBadRequest )
				response[:message] = 'Invalid token.'
			else
				response[:message] = 'Unable to connect to Facebook.'
			end

			render json: response
			return

		end

		data = JSON.parse res.body

		# Stop and destroy session if not logged in to Facebook
		if !data['status'] == 'connected'

			destroy_session

			response[:message] = 'Not logged to Facebook.'
			render json: response
			return

		end

		user = User.where( provider: 'facebook', external_id: data['id'] ).first_or_initialize do |user|

			user.provider = 'facebook'
			user.external_id = data['id']
			user.name = data['name']
			user.email = data['email']
			user.extra_data = res.body

			user.save!

		end

		user.update_column( 'last_login', DateTime.current )

		create_session( user )

		render json: {'success' => true} and return

	end

	def logout

		destroy_session

		render json: {'success' => true} and return

	end

	private

	def create_session( user )

		session[:user] = user.id

	end

	def destroy_session

		session[:user] = nil

	end

end
