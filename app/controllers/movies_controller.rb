class MoviesController < ApplicationController
	
	def show
		@movie = Movie.eager_load( :medias, :releases ).find_by_slug!( params[:slug] )
		
		# is it effective with heavy caching?
		#impressionist @movie
		
		#Desperate measure
		@movie.delay.fetch_external_data if ( @movie.description.blank? || @movie.medias.blank? ) && @movie.can_fetch_external_data? #!@movie.complete?
		
		@poster = @movie.medias.select {|media| media.media_type == 'poster'}.sample
		@backdrop = @movie.medias.select {|media| media.media_type == 'backdrop'}.sample
		
		# This will save a DB query. Crazy, right?
		@poster.movie = @movie unless @poster.blank?
		@backdrop.movie = @movie unless @backdrop.blank?
		@movie.releases.each {|release| release.owner = @movie} unless @movie.releases.blank?
		
		@canonical = url_for @movie
	
		respond_to do |format|
			format.html # show.html.erb
			format.json { render json: @movie }
		end
	end
	
	def all
		
		page = params[:page].to_i || 1
		page = 1 unless page > 0
		
		@movies = Movie.search_all( page )
		
		@canonical = url_for :action => :all
		@canonical << "?page=#{params[:page]}" if page > 1

	  
		respond_to do |format|
	      format.html # index.html.erb
	      format.json { render json: @movies }
	    end
	end
	
	def fetch_subtitles
	
		@movie = Movie.find_by_imdb_id( params[:id] )
		
		respond_to do |format|
		
			format.html { render layout: false }
			format.json { render json: @movie.releases }
		
		end
	
	end
	
end
