# encoding: utf-8
class SubtitlesController < ApplicationController

	respond_to :html, :json
	impressionist :actions => [:ping]

	def async_upload

		file = params[:file]
		obj = { :success => false }
		obj[:status] = SubtitleUtil.validate_subtitle( file.tempfile )

		case obj[:status]

			when 400 # Bad Request
				obj[:error] = 'Nenhum arquivo enviado.'

			when 413 # Request Entity Too Large
				obj[:error] = 'Arquivo muito grande. Tem certeza de que isso é uma legenda?'

			when 415 # Unsupported Media Type
				obj[:error] = 'Isso não parece ser uma legenda válida.'

			when 200 # OK
				obj[:success] = true

		end

		# go home early on error
		render json: obj, status: obj[:status] and return if status != 200

		params[:file] = obj[:file] = File.basename(file.tempfile) #remove tmpdir path. Security

		#copy file to rails tempdir if it's going to be processed later. Avoid system garbage collector
		FileUtils.mkdir_p Rails.root.join( 'tmp', 'uploads' )
		FileUtils.cp( file.tempfile, Rails.root.join( 'tmp', 'uploads', obj[:file] ) ) if params[:auto].blank?


		#batch upload, auto save subtitle
		if params[:auto] then

			data = SubtitleUtil.parse_subtitle_name( file.original_filename )
			media = SubtitleUtil.get_subtitle_media( data )

			if media.nil? then

				obj = {
					:success => false,
					:error => 'Não conseguimos descobrir de onde é essa legenda. Tente fazer o upload dela em separado e preencher os dados necessários no formulário.',
					:status => 412 #Precondition Failed
				}

			else

				params[:release] = {:name => File.basename( file.original_filename, '.srt' ) }

				obj = SubtitleUtil.save_subtitle( params, media, file.tempfile.path )
				obj[:status] = 400 if !obj[:success]

			end

		end

		render json: obj, status: obj[:status] and return
	end

	def upload

		@canonical = url_for :action => :upload

		obj = nil

		if request.post?

			if params[:media][:imdb_id]
				params[:media][:imdb_id] = params[:media][:imdb_id][/tt\d+/]
			end

			obj = SubtitleUtil.save_subtitle( params )

			@subtitles = obj[:subtitles]
			obj.delete :subtitles


		end # if (request.post)

		respond_to do |format|
			format.html {

				unless obj.blank?

					if obj[:success] then
						flash.now[:success] = true
					else
						flash.now[:error] = true
					end

				end

			}
			format.json { render( json: obj ) and return}
		end
	end

	# The download should be requested to the file server. we'll create a subtitle there.
	# To speed things up for the user, we'll serve the subtitle stored in db and then
	# put it to the file server.
	def download

		@subtitle = Subtitle.fetch params

		raise ActiveRecord::RecordNotFound if @subtitle.blank?

		iso88591_subtitle = @subtitle.text.encode( Encoding::ISO_8859_1, :undef => :replace, :invalid => :replace, :universal_newline => true )

 		send_data iso88591_subtitle,
 			:type => 'text/srt; charset=ISO-8859-1;',
 			:disposition => "attachment; filename=#{@subtitle.release.name}.srt"


 		unless Rails.application.config.file_server.blank?
	 		begin
				Net::HTTP.start( Rails.application.config.file_server ) {|http|

					request = Net::HTTP::Put.new( @subtitle.to_param )
					request.set_form_data( {'contents' => iso88591_subtitle} )
					response = http.request( request )

				}
			rescue Exception

				logger.info Exception

			end
		end # unless Rails.application.config.file_server.blank?


	end

	def ping

		@subtitle = Subtitle.find params[:id]

		impressionist @subtitle

		render json: {:success => true, :date => Time.now}

	end
end
