class MediasController < ApplicationController
	def show
		
		media_id = params[:name][/\d+$/]
		media = Media.find(media_id)
		
		media.build_thumb params[:dimension]

		redirect_to media.media_path params[:dimension]
	end
end