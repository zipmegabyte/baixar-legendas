class ApplicationController < ActionController::Base
	respond_to :html, :json, :xml

	before_filter :check_blacklist

	def index

		# The number of records to show
		show_featured_movies = Rails.application.config.show_featured_movies.to_i
		show_featured_tv_shows = Rails.application.config.show_featured_tv_shows.to_i

		# These are automatically discovered
		featured_movies = Settings.featured_movies || []
		featured_tv_shows = Settings.featured_tv_shows || []

		# These are fixed by the admin
		fixed_movies = Settings.featured_fixed_movies || []
		fixed_tv_shows = Settings.featured_fixed_tv_shows || []

		movies_to_show = ( fixed_movies + featured_movies ).uniq.first( show_featured_movies )
		tv_shows_to_show = ( fixed_tv_shows + featured_tv_shows ).uniq.first( show_featured_tv_shows )

		all_to_show = movies_to_show + tv_shows_to_show

		search = Sunspot.search( Movie, TvShow ) {

			with :imdb_id, all_to_show
			order_by :random

		}

		results = SolrSearch.build_result_set( search.hits )

		@movies = results[:movies]
		@tv_shows = results[:tv_shows]

		if @movies.size < show_featured_movies

			@movies += Movie.find_by_sql([
				"SELECT DISTINCT m.* FROM movie m
				 INNER JOIN `release` r ON m.id = r.owner_id AND r.owner_type = 'Movie'
				 WHERE m.slug NOT IN ( ? )
				 ORDER BY m.release_date DESC
				 LIMIT ?",
				 Settings.featured_movies,
				 show_featured_movies - @movies.length
			])

		end

		if @tv_shows.size < show_featured_tv_shows

			@tv_shows += TvShow.find_by_sql([
				"SELECT DISTINCT t.* FROM `release` r
				 INNER JOIN episode e ON e.id = r.owner_id AND r.owner_type = 'Episode'
				 INNER JOIN tv_show t ON t.id = e.tv_show_id
				 WHERE t.slug NOT IN ( ? )
				 ORDER BY e.release_date DESC
				 LIMIT ?",
				 Settings.featured_tv_shows,
				 show_featured_tv_shows - @tv_shows.length
			])

		end

		@no_search = true

		@canonical = url_for :action => :index

	end

	def search

		@search = SolrSearch.search params

		@canonical = search_url( params )

		respond_with(@search) do |format|
			format.html {

				#Make search smarter
				unless params[:page].to_i > 1

					redirect_to @search[:releases].first[:url] and return unless @search[:releases].blank?

					redirect_to @search[:episodes].first.episode_path and return unless @search[:episodes].blank?

					all = @search.values.flatten;
					redirect_to all.first.path and return if all.length == 1
				end

			}
			format.json {
				render json: {
					:movies => @search[:movies] || [],
					:tv_shows => @search[:tv_shows] || [],
					:episodes => @search[:episodes] || [],
					:releases => @search[:releases] || [],
					:success => true,
					:internal => true
				}
			}
		end

	end

	def external_search

		results = { :movies => [], :tv_shows => [], :episodes => [], :success => true, :internal => false }

		# Will try to get info for TvShow or Movie according to type.
		# If there's no type, but imdb_id, try both, returns who
		# matches first.
		if params[:type] == 'tv_show'

			tv_show = TvShow.new()
			tv_show.name = params[:name]
			tv_show.year = params[:year]

			tv_show.fetch_external_data_fast

			results[:tv_shows] << tv_show if tv_show.complete?

		elsif params[:type] == 'movie'

			movie = Movie.new()
			movie.name = params[:name]
			movie.year = params[:year]

			movie.fetch_external_data_fast

			results[:movies] << movie if movie.complete?

		elsif !params[:imdb_id].blank?

			movie = Movie.new()
			movie.imdb_id = params[:imdb_id]

			movie.fetch_external_data_fast

			if movie.complete?

				results[:movies] << movie

			else

				tv_show = TvShow.new()
				tv_show.imdb_id = params[:imdb_id]

				tv_show.fetch_external_data_fast

				if tv_show.complete?

				results[:tv_shows] << tv_show

				end

			end

		end

		respond_with(@search) do |format|
			format.json {
				render json: results
			}
		end

	end

	def sitemap


	end

	def short_url

		case params[:type]

			when 'm'
				cls = Movie

			when 't'
				cls = TvShow

			when 'e'
				cls = Episode

			when 'r'
				cls = Release

			when 's'
				cls = Subtitle

			else
				raise ActiveRecord::RecordNotFound

		end

		entity = cls.find( params[:id].to_i( 36 ) )

		redirect_to entity.path, :status => 301

	end

	def error

		render "error#{params[:error]}"

	end

	def maintenance

		render "maintenance"

	end

	private

	#
	# Checks weather this request is blacklisted (probably due to DMCA takedown
	# request)
	#
	def check_blacklist

		blacklist = Settings.blacklist || []

		raise ActionController::RoutingError.new('Not Found') if blacklist.include? request.path
	end
end
