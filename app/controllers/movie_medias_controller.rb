class MovieMediasController < ApplicationController
	def show
		
		media_id = params[:name][/\d+$/]
		media = MovieMedia.find(media_id)
		
		content = media.build_thumb( params[:dimension] )
		
		render( :text => content ) && return if !content.blank?
		
		redirect_to( media.media_path( params[:dimension] ), :status => :moved_permanently )

	end
end