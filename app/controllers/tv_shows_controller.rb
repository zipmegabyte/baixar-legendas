class TvShowsController < ApplicationController

	def show
		@tv_show = TvShow.find_by_slug( params[:tv_show_slug] )

		# Try to find TvShow splitting slug with year
		if @tv_show.blank? then

			slug_and_year = []
			match = params[:tv_show_slug].match /^(.+)[-_](\d{4})$/

			if match

				slug_and_year << match[1]
				slug_and_year << match[2]

			end

			@tv_show = TvShow.find_by_slug( slug_and_year[0] ) if slug_and_year.length == 2

			raise ActiveRecord::RecordNotFound if @tv_show.blank?
		end

		# is it effective with heavy caching?
		#impressionist @tv_show

		#Desperate measure
		@tv_show.delay.fetch_external_data if ( !@tv_show.description.blank? || @tv_show.medias.blank? ) && @tv_show.can_fetch_external_data? #!@tv_show.complete?

		@poster = @tv_show.medias.select {|media| media.media_type == 'poster'}.sample;
		@backdrop = @tv_show.medias.select {|media| media.media_type == 'backdrop'}.sample;

		@canonical = url_for @tv_show

		@episodes = @tv_show.episodes.order( 'season DESC, number DESC' ).eager_load( :releases )
		@seasons = {}

		@episodes.each do |episode|

			@episode = episode if @episode.blank? && !episode.releases.blank?


			if episode.season == 0 || episode.number == 0 || episode.release_date.blank? || episode.release_date.future?
				next if episode.releases.blank?
			end

			(@seasons[episode.season.to_s] ||= []) << episode

		end

		#Weird way of fetching the first episode from @seasons
		@episode = @seasons.first.last.first if @episode.nil? and !@seasons.empty?

		respond_to do |format|
			format.html
			format.json { render json: @tv_show }
		end
	end

	def all

		page = params[:page].to_i || 1
		page = 1 unless page > 0

		@tv_shows = TvShow.search_all( page )

		@canonical = url_for :action => :all
		@canonical << "?page=#{params[:page]}" if params[:page].to_i > 1

		respond_to do |format|
		  format.html
		  format.json { render json: @tv_shows }
		end

	end

end
