(function($){

	var app = {

		regexpTvShow: /^(?:.+\\)?((.+?)(?:[(\[]?((?:18|19|20)\d\d)[)\]]?)?[.\s_-](?:(?:s(\d\d)e(\d\d))|(?:(\d?\d)x(\d?\d))|(?:(\d)(\d\d)))[.\s_-]((?:108|72|48)0[pi])?.+)\.\w{3}$/i,
		regexpMovie: /^(?:.+\\)?((.+?)[(\[]?((?:18|19|20)\d\d)[)\]]?[.\s-_]?((?:108|72|48)0[pi])?.+)\.\w{3}$/i,
		regexpRelease:/^(.+)\.\w{3}$/,
		regexpImdbId:/.*(tt\d+).*/,
		imdbUrlFragment: 'http://www.imdb.com/title/',
		fromMovie: document.location.search.match(/imdb_id=tt\d+/),

		searchCache: {},

		parseSubtitleName: function( subtitleName ) {

			var match, data;

			if ( match = app.regexpTvShow.exec( subtitleName ) ) {

				data = {

					'type': 'tv_show',
					'release': match[1],
					'name': $.trim( match[2].replace( /[\._]/g, ' ' ) ),
					'year': match[3],
					'season': +match[4] || +match[6] || +match[8],
					'episode': +match[5] || +match[7] || +match[9]

				}
			}
			else if ( match = app.regexpMovie.exec( subtitleName ) ) {

				data = {

					'type': 'movie',
					'release': match[1],
					'name': $.trim( match[2].replace( /\./g, ' ' ) ),
					'year': match[3]

				}
			}
			else if ( match = app.regexpRelease.exec( subtitleName ) ) {

				data = {

					'release': match[1]

				}

			}

			return data;

		},

		selectSubtitleFile: function( subtitleName ) {

			var data = app.parseSubtitleName( subtitleName );

			if ( !data ) return;

			// came from movie page, contents already loaded
			if ( app.fromMovie ) {
				app.$release.val( data.release );
				return;
			}

			$.each( data, function( name, val ) {

				var el = app['$' + name];
				if ( !el ) return;

				if ( el.is( ':radio' ) ) {

					el.each(function() {
						$( this ).prop( 'checked', $( this ).val() == val );
					});

				}
				else {

					el.val( val );

				}

			});

			app.$type.filter( ':checked' ).trigger( 'change' );
			app.$release.trigger( 'keyup' );
		},

		showAndHideFormEls: function() {

			app.$type.each(function(){

				if ( $( this ).val() == 'tv_show' && $( this ).prop( 'checked' ) ) app.$tvShowOnly.show().find( ':input' ).attr( 'disabled', false );
				else if ( $( this ).val() == 'movie' && $( this ).prop( 'checked' ) ) app.$tvShowOnly.hide().find( ':input' ).attr( 'disabled', true );

			});

		},

		formResetExcluding: function( ) {

			var elsToReset;

			app.$itemInfo.empty();

			if ( !arguments.length ) {

				this.reset();
				return;

			}

			elsToReset = app.$form.find( ':input' );

			$.each( arguments, function(i, el) { elsToReset = elsToReset.not( el ); } );

			elsToReset.each(function(i, el) {

				if ( $( el ).is( ':radio' ) ) {
					$( el ).each( function() {
						$( this ).prop( 'checked', false );
					});
				}
				else {
					$( el ).val('');
				}

			});

		},

		getFromCache: function( params ) {

			return app.searchCache[$.param( params )];

		},

		addToCache: function ( params, search ) {

			app.searchCache[params] = search;

		},

		search: function( obj ) {
			var data, result;

			if ( app.$imdbId.is( obj.target ) ) {

				app.formResetExcluding( app.$file, app.$description, app.$season, app.$episode, app.$imdbId, app.$type, app.$release );

				data = {
					imdb_id: app.$imdbId.val().replace( app.regexpImdbId, '$1' ),
					rows: 1
				}

			}
			else {

				data = {
					type: app.$type.filter( ':checked' ).val(),
					name: app.$name.val(),
					year: app.$year.val(),
					rows: 1
				}

			}

			app.ajaxSearch( data );

		},

		ajaxSearch: function( data, uri ) {

			if ( !data.imdb_id && ( !data.type || !data.name ) ) return;

			// try hitting the cache
			result = app.getFromCache( data );

			if ( result ) {

				app.searchSuccess( result );
				return;

			}

			app.data = data;

			// the actual search (ajax)
			$.ajax( uri || '/search?best_match=true', {

				data: data,
				dataType: 'json',
				success: app.searchSuccess

			});

		},

		searchSuccess: function( response ) {

			var data, html;

			if ( !response ) return;

			data = app.translateResponse( response );

			if ( $.isEmptyObject( data ) && response.internal ) {

				app.ajaxSearch( app.data, '/external_search' );
				return;

			}

			if ( $.isEmptyObject( data ) ) return;

			if ( this.url ) app.addToCache( this.url.replace('/search?', ''), response );

			if ( data.imdb_id ) app.$imdbId.val( app.imdbUrlFragment + data.imdb_id );

			if ( !app.$type.filter( ':checked' ).length ) app.$type.filter( '[value=' + data.type + ']' ).prop( 'checked', true );
			if ( !app.$name.val() ) app.$name.val(data.name);
			if ( !app.$year.val() ) app.$year.val(data.year);
			if ( !app.$imdbId.val() ) app.$imdbId.val(data.imdb_id);

			html = '<h4>' + data.name;
			if ( data.year ) html += ' (' + data.year + ')';
			html += '</h4>';
			if ( data.description ) html += '<p class="muted"><small>' + data.description + '</small></p>';

			if ( data.media ) html = '<img src="' + data.media.url + '" class="img-responsive">' + html;

			app.$itemInfo.hide().html( html ).fadeIn( 'slow' );

			app.showAndHideFormEls();

		},

		translateResponse: function( response ) {

			var tvShow, movie, data = {};
			if ( !response ) return data;

			if ( response.tv_shows && response.tv_shows.length ) {

				data = response.tv_shows[0];
				data.type = 'tv_show';

			}
			else if ( response.movies && response.movies.length ) {

				data = response.movies[0];
				data.type = 'movie';

			}

			if ( data.medias == null || !data.medias.length ) return data;

			data.media = data.medias[0];

			data.media.url = 'http://img.legendas.de/' + ( data.type == 'movie' ? 'filme' : 'serie' ) + '/original/' + data.slug + '_' + data.media.id + '.jpg';

			return data;
		},

		growTextArea: function( e ) {
			$( this ).height( $( this ).data( 'height' ) );
			while( $( this ).outerHeight() < this.scrollHeight + parseFloat( $( this ).css( 'borderTopWidth' ) ) + parseFloat( $( this ).css( 'borderBottomWidth' ) ) ) {
				$(this).height( $(this).height() + 1 );
			}
		},

		uploadInit: function() {

			var uploader = new plupload.Uploader({
				runtimes: 'html5,gears,flash,silverlight,browserplus',
				flash_swf_url : '/js/plupload/js/plupload.flash.swf',
				silverlight_xap_url : '/js/plupload/js/plupload.silverlight.xap',
				url: '/async_upload',
				max_file_size: '1mb',
				unique_names: true,
				drop_element: 'drop-target',
				browse_button: 'upload-dialog',
				multi_selection: true,
				multiple_queues: false
			});

			uploader.init();

			app.cloudInit();

			uploader.bind( 'BeforeUpload', function(up, file) {

				if ( !up.settings.auto_upload ) app.selectSubtitleFile( file.name );

			});

			uploader.bind( 'FilesAdded', function(up, files) {

				app.animateCloud( up.total.percent );
				app.$cloudText.add( app.$cloudMask ).add( app.$cloudTspan ).css( 'display', 'block' );

				$('.alert').alert('close');

				up.settings.errors = [];
				// multiple files, auto upload
				if (files.length > 1) {
					up.settings.url = '/async_upload_auto'
					up.settings.auto_upload = true

					$( ':input', app.$form ).attr( 'disabled', 'disabled' );

				}
				else {
					up.settings.url = '/async_upload'
					up.settings.auto_upload = false
				}


				uploader.start();
				app.$uploadDialog.html( '<h6><strong>Enviando...</strong></h6><p></p>' );

			});

			uploader.bind( 'UploadComplete', function( up, files ) {

				$( ':input', app.$form ).removeAttr( 'disabled' );
				app.showAndHideFormEls();

				if ( up.settings.errors.length ) {
					app.uploaderError( up.settings.errors );
				}
				else {

					if ( up.settings.auto_upload ) {

						app.$uploadDialog.html( '<h6><strong>Obrigado!</strong></h6><h5>Você enviou as legendas em modo turbo e nem precisou preencher nada no formulário! Fique à vontade para enviar mais quando quiser.</h5>' );

					}
					else {

						app.$uploadDialog.html('<h6>O arquivo <strong>' + files[0].name + '</strong> foi recebido com sucesso.</h6><h5 class="text-muted"><strong>Viu como foi rápido?<br>Preencha o formulário ao lado e conclua o upload.</strong></h5>');

					}
				}

				up.splice();
				up.refresh();

			});

			uploader.bind( 'UploadFile', function( up, file ) {

				$( 'p', app.$uploadDialog ).text( file.name );

			});

			uploader.bind( 'FileUploaded', function( up, file, info ) {

				app.animateCloud( up.total.percent );

				var response = $.parseJSON( info.response );

				if ( response.success ) {

					app.$file.val( response.file );

				}

				else {

					file.msg = response.error;

				}

			});

			uploader.bind( 'Error', function( up, error ) {

				var msg = '';

				switch( error.code ) {

				case -200:

					switch( error.status ) {

					case 413:
						msg = 'O arquivo ' + error.file.name + ' é muito grande. Tem certeza de que isso é uma legenda?';
					break
					case 415:
						msg = 'O arquivo ' + error.file.name + ' não parece ser uma legenda válida.';
					break
					case 412:
						msg = 'Não conseguimos descobrir de onde é a legenda ' + error.file.name + '. Tente fazer o upload dela em separado e preencher os dados necessários no formulário.'
					break
					default:
						msg = 'Um erro interno impediu o envio da legenda ' + error.file.name + '. Por favor, tente novamente.';
					}

				break;
				case -600:
					msg = 'O arquivo ' + error.file.name + ' é muito grande. Tem certeza de que isso é uma legenda?'
				break
				}

				up.settings.errors.push( msg );

			});

		},

		uploaderError: function( errors ) {

			app.$cloudText.add( app.$cloudMask ).add( app.$cloudTspan ).css( 'display', 'none' );

			app.$uploadDialog.html('<h6><strong>Arraste e solte aqui as legendas que você deseja compartilhar!</strong></h6>');

			var errorMsg = '',
				alertHtml = '';
			$.each( errors, function( idx, error ) {
				errorMsg += '<li>' + error + '</li>'
			});

			alertHtml = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button><h4 class="alert-heading">Ops! Algo deu errado, confira abaixo:</h4><ul>' + errorMsg + '</ul></div>';

			$( '.alert' ).remove();

			app.$form.before( alertHtml );
		},

		cloudInit: function() {

			app.$cloud = $( '#cloud' )[0].contentDocument;
			app.$cloudMask = $('#mask', app.$cloud);

			//try to wait for svg to be ready
			if ( !app.$cloudMask.length ) return setTimeout( app.cloudInit, 100 );

			app.cloudClipHeight = $( '#clip rect', app.$cloud ).attr('height');
			app.cloudFrom = app.cloudClipHeight;

			app.$cloudMask.append($( 'g:first > *', app.$cloud ).clone());
			app.$cloudText = $('text', app.$cloud);
			app.$cloudTspan = $('tspan', app.$cloud);
			app.$cloudClip = $('#clip', app.$cloud);
			app.$cloudAnimation = $( 'animateTransform', app.$cloudClip );
			app.$cloudClip.attr( 'transform', 'translate(0,' + (app.cloudClipHeight) + ')' );
		},

		animateCloud: function( percent ) {

			var to = app.cloudClipHeight - app.cloudClipHeight * percent / 100,
				duration = Math.abs( to - app.cloudFrom ) / 100;

			if ( !percent ) app.cloudFrom = to;

			app.$cloudTspan.text( percent + '%' );
			app.$cloudAnimation.attr( 'from', '0,' + app.cloudFrom );
			app.$cloudAnimation.attr( 'to', '0,' + to );
			app.$cloudAnimation.attr( 'dur', duration + 's' );
			app.$cloudAnimation[0].beginElement();

			app.cloudFrom = to;
		},

		domReady: $(function() {

			//cache DOM elements and bind events
			app.$type = $( ':radio[name=type]' ).change( app.showAndHideFormEls ).change( app.search );
			app.$release = $( '#release' ).each(function() {$( this ).data( 'height', $(this).height() ) }).keyup( app.growTextArea ).tooltip();
			app.$name = $( '#name' ).change( app.search );
			app.$season = $( '#season' );
			app.$episode = $( '#episode' );
			app.$year = $( '#year' ).change( app.search );
			app.$description = $( '#description' );
			app.$form = $( '.form-info' );
			app.$tvShowOnly = $( '.tv-show-only' );
			app.$imdbId = $( '#imdb-id' ).change( app.search );
			app.$itemInfo = $( '.item-info' );
			app.$file = $( '.form-info :input[name = file]' );
			app.$button = $( '.form-info button' );
			app.$uploadDialog = $( '.upload-dialog' );

			if ( app.$imdbId.val() ) app.$imdbId.trigger( 'change' );

			app.uploadInit();

			appl = app;

		})

	}

})( window.jQuery );
