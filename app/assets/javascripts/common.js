app = (function($){

    // Image lazy load and failsafe.
    function lazyLoad() {

        $( 'img' ).each(function() {

            $( this ).data( 'placeholder', $(this).attr( 'src' ) );

        }).error(function() {

            $( this ).attr( 'src', $(this).data( 'placeholder' ) );

        }).unveil();

    }

    // Track subtitle downloads on GA
    function trackDownload() {

        $( 'a[href$=srt]' ).click(function(){

            ga('send', 'event', 'Subtitles', 'Download', $( this ).attr( 'href' ));
            $.getJSON( '/direct/legenda/ping/' + $( this ).data( 'ping' ) );

        })

    }

    /*
    function displayLoggedUserInfo( response ) {

        if ( response.status === 'connected' ) {

            FB.api('/me', function( response ) {

                $( '.user-info span' ).html('<img src="https://graph.facebook.com/' + response.id + '/picture?width=20&height=20">' + response.first_name);

                $( '.not-logged-in' ).hide();
                $( '.logged-in' ).show();

            });

        }
        else {

            $( '.not-logged-in' ).show();
            $( '.logged-in' ).hide();

            $( '.user-info span' ).empty();

        }

    }

    function login( response ) {

        displayLoggedUserInfo( response );

        if ( response.status === 'connected' ) {

            $.getJSON( '/login', {
                token: response.authResponse.accessToken
            });

        }

    }

    function logout( e ) {

        if ( e ) {
            e.stopPropagation();
            e.preventDefault();
        }

        $.getJSON( '/logout', function() {FB.logout(displayLoggedUserInfo)} );
    }

    $( '.login' ).click( function() {FB.login( login, {scope: 'email'} )} );
    $( '.logout' ).click( logout );

    window.fbAsyncInit = function() {
        FB.Event.subscribe( 'auth.statusChange', function() {FB.getLoginStatus( displayLoggedUserInfo )} );
        FB.init( window.fbSetup );
    };
    */

    // On DOM ready
	$(function() {
		lazyLoad();
		trackDownload();
	});

    return {}

})( window.jQuery || window.Zepto );
