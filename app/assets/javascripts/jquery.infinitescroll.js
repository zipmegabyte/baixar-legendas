(function($) {

	$.fn.infinitePagination = function() {

		var totalPages,
			currentPage,
			paginationBaseLink,
			itemBaseLink,
			imgBaseLink,
			template,
			loader,
			working = false,
			staticUrl = $( this ).data( 'static-url' ) || '',
			o = {
				itemsList: $( '.list' ),
				srcImg: staticUrl + '/assets/poster-bg.svg',
				loadingImg: staticUrl + '/assets/ajax-loader.gif'
			},
			_throttleTimer = null,
			_throttleDelay = 10,
			entity = $( '.thumbnail' ).attr( 'href' ).substring(0, 7);

		/**
		 * To be bound to .each
		 */
		function init( i, el ) {


			totalPages = $( 'li', el ).last().prev().text() * 1;
			currentPage = $( 'li.active', el ).text() * 1;

			if ( currentPage >= totalPages ) return;

			//preload gif
			loader = $( '<p class="text-center"><img src="' + o.loadingImg + '"></p>' );

			template = $( '>:first-child', o.itemsList ).clone();

			paginationBaseLink = $( 'li a', el ).prop( 'href' ).replace( /\d+$/, '');
			itemBaseLink = $( 'a', template ).attr( 'href' ).replace( /[^\/]+$/, '');
			imgBaseLink = 'http://img.legendas.de' + itemBaseLink + '160x240/'

			el.remove();

			watchScroll();

		}

        function watchScroll() {

        	$(window)
				.off('scroll', scrollHandler)
				.on('scroll', scrollHandler);

        }

        function scrollHandler(e) {
			//throttle event:
			clearTimeout(_throttleTimer);
			_throttleTimer = setTimeout(function () {

				//do work
				if ($(window).scrollTop() + $(window).height() > $(document).height() - 200) {

					if ( working || currentPage >= totalPages ) return;
					loadPage( currentPage + 1 );

				}

			}, _throttleDelay);
		}

		function buildPage( data ) {

			var elements = [],
				itemsOnPage = $('> .item', o.itemsList).length;

			$.each( data, function( i, el ) {

				var clone = template.clone(),
					current = i + itemsOnPage + 1;

				$( 'a', clone ).attr( 'href', entity + el.slug );
				$( 'img', clone )
					.attr( 'alt', buildAlt( el ) )
					.attr( 'src', o.srcImg )
					.attr( 'data-src', o.srcImg );

				if (el.medias.length && el.medias[0].id) $( 'img', clone ).attr( 'data-src', buildDataSrc( el ) );

				$( 'h3', clone ).html( buildTitle( el ) );

				elements.push( clone );

				if ( current % 4 == 0 ) elements.push( '<div class="clearfix visible-sm-block"></div>' );
				if ( current % 6 == 0 ) elements.push( '<div class="clearfix visible-lg-block"></div>' );

			});

			o.itemsList.append( elements ).find( 'img' ).unveil();

			currentPage += 1;

		}

		function buildTitle( el ) {

			return el.name + ( el.year ? ' <span class="text-muted text-normal">(' + el.year + ')</span>' : '' );

		}

		function buildDataSrc( el ) {

			return imgBaseLink + el.slug + '_' + el.medias[0].id + '.jpg';

		}

		function buildAlt( el ) {

			return el.name + ( el.year ? ' (' + el.year + ')' : '' );

		}

		function loadPage( page ) {

			working = true;
			o.itemsList.after( loader );

			$.ajax({
				type: 'get',
				contentType: "application/json",
				url: paginationBaseLink + page + '.json',
				dataType: "json",
				success: buildPage,
				complete: function() {
					working = false;
					loader.detach();

					ga( 'send', 'pageview', {
						page: paginationBaseLink + page,
						title: document.title.replace(/\(\d+/, '(' + page)
					});
				}
			});


		}

		return $( this ).each( init );
	};

	$( '.pagination' ).infinitePagination();

}( window.jQuery ));
