(function($){

	var app = {

		domReady: $(function() {

			app.fitSubtitleNames();

			app.fetchSubtitles();

		}),

		fitSubtitleNames: function() {

			var $subtitles = $( '.subtitles-list li' ),
				subtitlesHeight,
				subtitlesWidth;

			subtitlesHeight = $subtitles.height() + 1;
			subtitlesWidth = $subtitles.width();

			$subtitles.each(function(){

				var $a = $( this ).find( 'a' ),
				fontSize = 100;

				while ( ( $a.height() > subtitlesHeight || $a.width() > subtitlesWidth ) && fontSize) $a.css('font-size', (fontSize--) + '%');
				// ellipsis if font < 10px
				if ( parseInt( $a.css( 'font-size' ), 10 ) < 11 ) {

					$a.css( 'font-size', '11px' );

					while ( $a.height() > subtitlesHeight && fontSize) $a.html( '&hellip;' + $a.html().substring( 2 ) );
				}
			});

		},

		fetchSubtitles: function() {

			var $progress = $('.panel-subtitles .progress'),
				$list = $( '.subtitles-list' );

			$list.each(function(){

				var id = $( this ).data( 'id' ),
					type = $( this ).data( 'type' );

				if ( !id ) return;
				$.ajaxSetup({global:false});
				$.getJSON( 'http://legendas.de/fetch_subtitles/index.php?callback=?', {imdb_id: id} );

				$list.data( 'originalMsg', $list.html() );
				$list.html( '<p>Aguarde, estamos procurando suas legendas</p>' );
				$progress.removeClass('hidden');
				app.pingForSubtitles( $list, $progress, type + '/' + id, 20, 3 );

			});

		},

		pingForSubtitles: function( $list, $progress, id, retries, retriesWithoutSuccess ) {

			if ( !$( 'ul', $list ).length && !retriesWithoutSuccess ) {

				app.restoreOriginalMessage( $list, $progress );

				return;

			}

			if ( !retries ) {

				if ( !$( 'ul', $list ).length ) app.restoreOriginalMessage( $list, $progress );

				return;

			}

			window.setTimeout( function(){
				$.get( '/direct/buscar-legendas/' + id, function( data ) {

					if ( data ) {

						$list.html( data );
						app.fitSubtitleNames();

					}

					app.pingForSubtitles( $list, $progress, id, --retries, --retriesWithoutSuccess );

				});

			}, 5e3 );

		},

		restoreOriginalMessage: function( $list, $progress ) {

			$list.html( $list.data( 'originalMsg' ) );
			$progress.addClass('hidden');

		}

	}

})( window.jQuery || window.Zepto );
