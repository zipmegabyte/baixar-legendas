# encoding: utf-8
class TvShowMedia < ActiveRecord::Base
  belongs_to :tv_show

  before_save :calculate_hash
  after_save :after_save

  def tv_show_media_path( dimension = 'original' )
    #"/media/serie/#{dimension}/#{tv_show.slug}_#{id}.jpg"
    "http://#{Rails.application.config.image_server}/serie/#{dimension}/#{tv_show.slug}_#{id}.jpg"
  end
  
  def media_path( dimension = 'original' )
    tv_show_media_path dimension
  end

  def build_thumb( dimension )

    response = nil
  
  	Net::HTTP.start( Rails.application.config.image_server ) {|http|
		
		request = Net::HTTP::Put.new( URI( self.media_path dimension ).path )
		request.set_form_data( {'remote_url' => self.url} )
		response = http.request( request )
		
	}

	return response.body if response.is_a?( Net::HTTPSuccess )
  end
  
  def owner
  
  	tv_show
  
  end
  
  def owner=( tv_show )
  	
  	self.tv_show = tv_show
  	
  end
  
  def after_save
  
  	if url_changed?
		self.build_thumb( '160x240' )
		self.build_thumb( '215' )
		self.build_thumb( '770x370' )
		self.build_thumb( '338x190' )
		self.build_thumb( 'original' )
	end
	
  end

  private
  def calculate_hash
    require 'digest/md5'
    self.url_hash = Digest::MD5.digest(self.url)
  end

  handle_asynchronously :build_thumb
end
