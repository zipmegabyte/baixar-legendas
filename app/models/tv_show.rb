# encoding: utf-8
class TvShow < ActiveRecord::Base
	#include FulltextSearch
	include TvShowExternal
	include SolrProduction
	include ShortUrl

  is_impressionable #:counter_cache => {:unique => true}

  has_many :episodes, -> { order( season: :desc, number: :desc ) }, :dependent => :destroy
  has_many :medias, :dependent => :destroy, :class_name => 'TvShowMedia'

  validates :slug, :uniqueness => true, :allow_nil => true
  validates :imdb_id, :uniqueness => true, :allow_nil => true
  validates :thetvdb_id, :uniqueness => true, :allow_nil => true

  validates_each :imdb_id, :thetvdb_id do |record, attr, value|

  	record.errors.add(attr, 'Você precisa fornecer um identificador para esta série.') if record.imdb_id.blank? and record.thetvdb_id.blank?

  end

  after_update :purge_cache

  before_save :prepare_for_save

#   searchable do
#   	text :name, :description
#   	integer :year
#   	string :imdb_id
#   end

  def to_param
    self.slug
  end

  def as_json( options = {} )
  	super({
  		:only => [:name, :year, :slug, :imdb_id],
  		:include => [
  			:medias => {:only => [:id]}
  		]
  	})
  end

  def tv_show_path
    "/serie/#{self.to_param}"
  end

  def path
    tv_show_path
  end

  def num
  	"#{"%02d" % self.number}"
  end

  def prepare_for_save

  	self.build_slug
  	self.fix_name

  end

  def fix_name

  	m = /^(.+)( \(#{self.year}\))/.match self.name

  	self.name = m[1] unless m.nil?

  end

  def random_poster

		return nil if self.medias.empty?

		poster = self.medias.select {|media| media.media_type == 'poster'}.sample

		return nil if poster.nil?

		return poster

	end

	def poster( size = '160x240' )

		poster = random_poster

		return nil if poster.nil?

		return poster.media_path size

	end

  #compatibility
  def to_s( type = :short )

	  self.name

  end

  def build_slug

  	slug = self.name.parameterize
  	exists = false

  	if self.id.blank?
  		exists = TvShow.where( :slug => slug ).exists?
  	else
  		exists = TvShow.where( :slug => slug ).where( 'id <> ?', self.id ).exists?
  	end

  	if !exists
  		self.slug = slug
  	else
  		self.slug = "#{self.name.parameterize}_#{self.year}"
  	end

  end

  def update_latest_episode

  	episode = self.episodes.joins( 'INNER JOIN `release` ON episode.id = release.owner_id AND release.owner_type = "Episode"' ).order( 'season DESC, number DESC' ).limit( 1 )

  	return if episode.blank?

  	hierarchy = episode[0].hierarchy

  	return if self.latest_episode == hierarchy

  	self.latest_episode = hierarchy
  	self.save

  end

  # def get_latest_episode
#
#   	Episode.find_by_sql(
#   	"SELECT *
# 	 FROM episode ep
# 	 WHERE ep.season = (SELECT MAX(season) FROM episode WHERE tv_show_id = ep.tv_show_id)
# 	 AND ep.number = (SELECT MAX(number) FROM episode WHERE tv_show_id = ep.tv_show_id AND season = (SELECT MAX(season) FROM episode WHERE tv_show_id = ep.tv_show_id))
# 	 AND ep.tv_show_id = 1175
# 	 LIMIT 1")
#
#   end

  # Class Methods

  def self.from_uploaded_data( data )

  	tv_show = TvShow.search( data.merge :best_match => true ).first

  	unless tv_show

  		tv_show = TvShow.new()
  		tv_show.name = data[:name]
		tv_show.year = data[:year]
		tv_show.imdb_id = data[:imdb_id]

  	end

  	return tv_show

  end

  def purge_cache

  	Cloudflare.purge self.path

  end

end
