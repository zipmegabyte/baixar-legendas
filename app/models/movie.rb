# encoding: utf-8
class Movie < ActiveRecord::Base
	#include FulltextSearch
	include MovieExternal
	include SolrProduction
	include ShortUrl

	is_impressionable #:counter_cache => {:unique => true}

	has_and_belongs_to_many :series, :join_table => :movie_series
	has_many :releases, as: :owner
	has_many :medias, :class_name => 'MovieMedia'

	validates :slug, :uniqueness => true, :allow_nil => true
	validates :imdb_id, :uniqueness => true, :allow_nil => true
	validates :themoviedb_id, :uniqueness => true, :allow_nil => true

	validates_each :imdb_id, :themoviedb_id do |record, attr, value|

		record.errors.add(attr, 'Você precisa fornecer um identificador para este filme.') if record.imdb_id.blank? and record.themoviedb_id.blank?

	end

	before_save :build_slug
	after_update :purge_cache

	attr_accessor :subtitles_count

	@searchable = [:name, :original_name]

	def to_param
		slug
	end

	def movie_path
		"/filme/#{self.to_param}"
	end

	def path
		movie_path
	end

	def build_slug

		if self.year
			self.slug = "#{self.name.parameterize}_#{self.year}"
		else
			self.slug = self.name.parameterize
		end
	end

	def random_poster

		return nil if self.medias.empty?

		poster = self.medias.select {|media| media.media_type == 'poster'}.sample

		return nil if poster.nil?

		return poster

	end

	def poster( size = '160x240' )

		poster = random_poster

		return nil if poster.nil?

		return poster.media_path size

	end

	def subtitles_count()
		self[:subtitles_count] || self.releases.count
	end

	def as_json(options={})
		super({
	  		:only => [:name, :year, :slug, :imdb_id],
	  		:include => [
	  			:medias => {:only => [:id]}
	  		]
	  	})
	end

	#compatibility
	def to_s( type = :short )

		self.name

	end

	# Class Methods

	def self.from_uploaded_data( data )

		movie = Movie.search( data.merge :best_match => true ).first

		unless movie

			movie = Movie.new()
			movie.name = data[:name]
			movie.year = data[:year]
			movie.imdb_id = data[:imdb_id]

		end

		return movie

	end

	def purge_cache

		Cloudflare.purge self.path

	end

end
