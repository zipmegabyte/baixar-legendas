class Subtitle < ActiveRecord::Base
	include ShortUrl

	belongs_to :release
	belongs_to :team
	belongs_to :subtitle_text

	is_impressionable #:counter_cache => {:unique => true}

	validates :text, :presence => true
	validates :release_id, :uniqueness => true

	def to_param
		"#{release.to_param}.srt"
	end

	def subtitle_path
		unless Rails.application.config.file_server.blank?
			"http://#{Rails.application.config.file_server}#{self.to_param}"
		else
			self.to_param
		end
	end

	def path

		self.subtitle_path

	end

	def text

		self.subtitle_text.text

	end

	def text=(t)

		t = SubtitleUtil.sanitize( t )

		self.subtitle_text = SubtitleText.where( :md5 => Digest::MD5.hexdigest( t ) ).first_or_initialize( :text => t )

	end

	def self.fetch( params )

		subtitle = nil

		if params[:movie_slug]

			subtitle = self.find_by_sql([
				"SELECT subtitle.* FROM `subtitle`
				INNER JOIN `release` ON subtitle.release_id = release.id
				INNER JOIN `movie` ON release.owner_id = movie.id AND release.owner_type = 'Movie'
				WHERE release.name = ?
				AND movie.slug = ?
				LIMIT 1", params[:release], params[:movie_slug]]).first

		elsif params[:tv_show_slug]

			season = params[:season].sub( /\D/, '' ).to_i( 10 )
			number = params[:number].sub( /\D/, '' ).to_i( 10 )

			subtitle = self.find_by_sql([
				"SELECT subtitle.* FROM `subtitle`
				INNER JOIN `release` ON subtitle.release_id = release.id
				INNER JOIN `episode` ON release.owner_id = episode.id AND release.owner_type = 'Episode'
				INNER JOIN `tv_show` ON episode.tv_show_id = tv_show.id
				WHERE release.name = ?
				AND episode.season = ?
				AND episode.number = ?
				AND tv_show.slug = ?
				LIMIT 1", params[:release], season, number, params[:tv_show_slug]]).first

		end

		return subtitle
	end

end
