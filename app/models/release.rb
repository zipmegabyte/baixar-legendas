class Release < ActiveRecord::Base
	include ShortUrl

  belongs_to :owner, polymorphic: true
  has_one :subtitle, :dependent => :destroy
  
  before_save :build_slug
  after_save :update_latest_episode_in_tv_show
  after_create :after_create
  after_update :purge_cache
  
  searchable do
  
  	text :name
  	string( :path, :as => 'path_stored_string', :stored => true ) { |e| e.path }
  
  end
  
  def to_param
  	return "#{owner.path}/#{name}"
  end
  
  def release_path
  	self.to_param
  end

  def path
  	self.release_path
  end
  
  def to_s
  	name
  end
  
  def build_slug
  	self.slug = self.name.parameterize
  end
  
  def update_latest_episode_in_tv_show
  
  	episode = self.owner
  	return if 'Episode' != episode.class.name
  	
  	if episode.tv_show.latest_episode.blank? or episode.tv_show.latest_episode < episode.hierarchy
  	
  		episode.tv_show.latest_episode = episode.hierarchy
  		episode.save
  		
  		Cloudflare.purge
  	
  	end
  
  end
  
  def after_create
  
  	self.owner.purge_cache
  	 
  end
  
  def purge_cache
  
  	Cloudflare.purge self.path
  
  end

end
