# encoding: utf-8
class Episode < ActiveRecord::Base
	include EpisodeExternal
	include ShortUrl

  is_impressionable #:counter_cache => {:unique => true}
  
  belongs_to :tv_show
  has_many :releases, as: :owner
  
  validates :number, :uniqueness => {:scope => [:season, :tv_show_id]}
  
  after_create :after_create
  after_update :purge_cache
  after_save :after_save
    
  @order = :date
  
  def hierarchy
  	"S#{formatted_season}E#{formatted_number}"
  end
  
  def to_param
  	self.tv_show.to_param
  end
  
  def episode_path

    return "#{self.episode_path_simple}_#{self.name.parameterize}" if !self.name.nil?
    return self.episode_path_simple

  end
  
  def path
  	episode_path
  end
  
  def episode_path_simple
  	"#{self.tv_show.tv_show_path}/#{self.hierarchy}"
  end
  
  def formatted_number
   "#{"%02d" % number}"
  end
  
  def formatted_season
	  "#{"%02d" % season}"
  end
  
  def thumb_path( dimension = 'original' )
    "http://#{Rails.application.config.image_server}/episodio/#{dimension}/#{self.tv_show.slug}_#{self.hierarchy}.jpg" unless self.thumb.blank?
  end

  def build_thumb( dimension )
  
  	return if self.thumb.blank?
  	
  	response = nil
  
  	Net::HTTP.start( Rails.application.config.image_server ) {|http|
		
		request = Net::HTTP::Put.new( URI( self.thumb_path dimension ).path )
		request.set_form_data( {'remote_url' => self.thumb} )
		response = http.request( request )
		
	}

	return response.body if response.is_a?( Net::HTTPSuccess )
  end
  
  def subtitles_count()
		self.releases.count
	end
  
	def to_s( type = :short )
	
		str = hierarchy
		str << " - #{name}" if name
		
		str = "#{tv_show.name} " << str if tv_show and type == :long
		
		return str 
	end
	
	def purge_cache
		
		Cloudflare.purge [self.path, self.episode_path_simple]
		
	end
	
	def after_create
	
		self.tv_show.purge_cache
	
	end
	
	def after_save
	
		if thumb_changed?
			self.build_thumb( '770x370' )
			self.build_thumb( '338x190' )
			self.build_thumb( 'original' )
		end
	
	end
	
	handle_asynchronously :build_thumb
end
