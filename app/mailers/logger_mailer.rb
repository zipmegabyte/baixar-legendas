class LoggerMailer < ActionMailer::Base
  default from: "admin@baixarlegendas.com"
  
  def errors( errors, date )
  
  	@errors = errors
  	@date = date
  
  	mail(to: 'leonardotsr@gmail.com', subject: 'BaixarLegendas Crawler Errors')
  
  end
  
  def successes( successes, date )
  
  	@successes = successes
  	@date = date
  
  	mail(to: 'leonardotsr@gmail.com', subject: 'BaixarLegendas Subtitles sent')	
  
  end
end
