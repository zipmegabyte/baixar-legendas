xml.instruct! :xml, :version => '1.0', :encoding => 'UTF-8'


xml.urlset :xmlns => 'http://www.sitemaps.org/schemas/sitemap/0.9' do
  
  base_url = 'http://www.baixarlegendas.com.br'
  
  #home
  xml.url do
  	xml.loc base_url
  end
  
  #highlighted movies
  xml.url do
  	xml.loc base_url + '/filmes'
  	xml.changefreq 'hourly'
  end
  
  #all movies
  xml.url do
  	xml.loc base_url + '/filmes/todos'
  end
  
  #highlighted tv shows
  xml.url do
  	xml.loc base_url + '/series'
  	xml.changefreq 'hourly'
  end
  
  #all tv shows
  xml.url do
  	xml.loc base_url + '/series/todas'
  end
  
  #upload
  xml.url do
  	xml.loc base_url + '/upload'
  	xml.changefreq 'monthly'
  end
  
  #movies
  Movie.find_each( :batch_size => 500 ) do |movie|
    xml.url do 
      xml.loc( base_url + url_for( movie ) )
      xml.changefreq 'hourly'
    end
  end

  
  #tv_shows and episodes
  TvShow.find_each( :batch_size => 500, :include => :episodes ) do |tv_show|
    xml.url do 
      xml.loc( base_url + url_for( tv_show ) )
      xml.changefreq 'hourly'
    end
    
    tv_show.episodes.each do |episode|
    	xml.url do 
    		xml.loc( base_url + url_for( episode.episode_path ) )
    		xml.changefreq 'daily'
    	end
    end
  end
end