#! /usr/bin/env bash
 
# Loading the RVM Environment files.
source /opt/bitnami/rvm/environments/ruby-2.1.1@global
 
# Changing directories to your rails project.
cd /home/bitnami/apps/baixar-legendas
 
# Call Rake task
RAILS_ENV=development foreman run rake -s sitemap:refresh
