<?php
//@apache_setenv('no-gzip', 1);
//@ini_set('zlib.output_compression', 0);
//@ini_set('implicit_flush', 1);
date_default_timezone_set('America/Sao_Paulo');

require 'vendor/Encoding.php';
use ForceUTF8\Encoding;

$login = 'baixarlegendas-site';
$password = 'hilexagab';

function downloadPackage( $link ) {
		
	$ch = openConnection( $link );
	
	curl_setopt( $ch, CURLOPT_HEADER, 0 );
	
	$package = curl_exec( $ch );
	$headers = curl_getinfo( $ch );
	
	curl_close( $ch );
	
	if( $headers['http_code'] != 200 ) {
	
		echo "Unable to download subtitle {$headers['url']} due to error {$headers['http_code']}.";
		continue;
		
	}
	
	$packageName = getPackageName( $link, $headers['content_type'] );
	
	$packageFile = fopen( $packageName, 'wb' );
	fwrite( $packageFile, $package );
	fclose( $packageFile );
	
	return $packageName;
}

function saveDownloadInfoToLog( $imdb_id ) {
	
	$data = getRequestLog();
	
	if ( empty( $data ) ) $data = array();
	
	$data[$imdb_id] = strtotime( 'now' );
	
	file_put_contents('requests.json', json_encode( $data ) );
	
}

function getPackageName( $link, $contentType ) {
	
	$extension = getExtension( $contentType );
	$prefix = getPrefix( $link );
	
	$packageName = "packages/$prefix$extension";
	
	return $packageName;
	
}

function getPrefix( $link ) {
	
	$parts = explode( '/', $link );
	
	return end( $parts );
	
}

function getExtension( $contentType ) {
	
	$extension = '';
	if ( $contentType ) {
		
		$content = explode( '/', $contentType );
		
		if ( isset( $content[1] ) ) {
			
			$extension = ".$content[1]";
			
		}
		
	}
	
	return $extension;
	
}

function uploadSubtitlesInPackage( $packageName, $type, $imdb_id ) {
	
	switch ( pathinfo( $packageName, PATHINFO_EXTENSION ) ) {
		
		case 'zip':
			return unzipPackage( $packageName, $type, $imdb_id );
		case 'html':
			return @unlink( $packageName );
		
	}
	
}

function unzipPackage( $packageName, $type, $imdb_id ) {
	
	$zip = new ZipArchive;
	if ( !$zip->open( $packageName ) ) {
		
		echo 'Unable to open package zipfile.';
		@unlink( $packageName );
		
		return;
		
	}

	for( $i = 0; $i < $zip->numFiles; $i++ ) {
	
		$file = $zip->getNameIndex( $i );
	
		if ( pathinfo( $file, PATHINFO_EXTENSION ) != 'srt' ) continue;
	
		$releaseName = pathinfo( $file, PATHINFO_BASENAME );
		$subtitleText = Encoding::toUTF8( $zip->getFromIndex( $i ) );
	
		uploadSubtitle( $releaseName, $subtitleText, $type, $imdb_id );
	}
	
}

function uploadSubtitle( $releaseName, $subtitleText, $type, $imdb_id ) {
	
	$ch = openConnection( 'http://www.baixarlegendas.com.br/upload' );
	
	$data = array(
	
		'release[name]' => basename( $releaseName, '.srt' ),
		'subtitle[text]' => $subtitleText,
		'subtitle[description]' => 'OpenSubtitles',
		'media[imdb_id]' => $imdb_id,
		'type' => $type
		
	);
	
	curl_setopt( $ch, CURLOPT_POST, 1 );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, array2qs( $data ) );
	curl_setopt( $ch, CURLOPT_HEADER, 0);
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 
		'Accept: application/json',
		'Content-Type: application/x-www-form-urlencoded; charset=utf-8'
	));
	
	$result = fetch( $ch );
	
	return $result;
	
}

function array2qs( $arr ) {
	
	$elements = array();
	
	foreach( $arr as $key => $val ) {
		
		$elements[] = rawurlencode( $key ) . '=' . rawurlencode( $val );
		
	}
	
	$qs = implode( '&', $elements );
	
	return $qs;
	
}

function fetchXml( $imdbId ) {
	
	//$ch = openConnection( "http://www.opensubtitles.org/en/search/sublanguageid-pob/imdbid-$imdbId/xml" );
	
	//$result = fetch( $ch );
	
	//if ( !isLogged( $result ) ) 
	$result = login( $imdbId );
	
	return $result;
	
}

function getPackageLinks( $xml ) {
	
	preg_match_all("/LinkDownload='(http:\/\/dl\.opensubtitles\.org\/en\/download\/subad\/\d+)'/", $xml, $matches);
	
	return $matches[1];
	
}

function getProductionType( $xml ) {
	
	preg_match_all( '/<MovieKind>(movie|episode)<\/MovieKind>/', $xml, $matches );
	
	return end( $matches[1] );
	
}

function isLogged( $result ) {
	
	global $login;
	
	return preg_match( "/$login/", $result ) == 1;
	
}

function login( $imdbId, $tries = 3 ) {
	
	global $login, $password;

	$ch = openConnection( "http://www.opensubtitles.org/en/login/redirect-|en|search|sublanguageid-pob|imdbid-$imdbId|xml" );
	
	$postString = "a=login&redirect=%2Fen%2Fsearch%2Fsublanguageid-pob%2Fimdbid-$imdbId%2Fxml&user=$login&password=$password&remember=on";
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $postString );
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	
	$result = fetch( $ch );

	if ( !isLogged( $result ) ) {
		
		if ( $tries > 0 ) return login( $imdbId, $tries - 1 );

	}

	return $result;
	
}

function openConnection( $url ) {
	
	$ch = curl_init();
	
	curl_setopt( $ch, CURLOPT_URL, $url );
	configureConnection( $ch );
	
	return $ch;
	
}

function fetch( $ch ) {
	
	$result = curl_exec( $ch );
	
	curl_close( $ch );
	
	return $result;
	
}

function configureConnection( $ch ) {
	
	curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)' );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
	curl_setopt( $ch, CURLOPT_COOKIEJAR, 'cookie.txt' );
	//curl_setopt( $ch, CURLOPT_COOKIEFILE, 'cookie.txt' );
	curl_setopt( $ch, CURLOPT_COOKIESESSION, TRUE );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, TRUE );
	curl_setopt( $ch, CURLOPT_ENCODING, 'UTF-8' );
	curl_setopt( $ch, CURLOPT_HEADER, FALSE );
	curl_setopt( $ch, CURLOPT_HTTPGET, TRUE );
	curl_setopt( $ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
	
}

function respond( $errorMessage ) {
	
	$success = empty( $errorMessage );
	$response = array( 'success' => $success );
	if ( !$success ) {
		
		$response['error'] = $errorMessage;
		
	}
	
	$callback = $_REQUEST['callback'];
	
	header("Connection: close");
	ob_start();
	$json = json_encode( $response );
	
	if ( $callback ) {
		
		$json = "$callback($json)";
		
	}
	
	echo $json;
	
	$size = ob_get_length();
	header( "Content-Length: $size" );
	ob_end_flush();
	flush();
	
	if ( !$success ) die();
}

function validateRequest( $imdb_id ) {
	
	if ( !imdbParamOk( $imdb_id ) ) {
		return 'Parameter imdb_id is required.';
	}
	
	if ( !requestAllowed( $imdb_id ) ) {
		return "Too soon to make a new request for subtitles for $imdb_id.";
	}
	
}

function imdbParamOk( $imdb_id ) {
	
	return !empty( $imdb_id );
	
}

function requestAllowed( $imdb_id ) {
	
	$data = getRequestLog();
	
	if ( empty( $data ) || empty( $data[$imdb_id] ) ) return true;
	
	return $data[$imdb_id] < strtotime( 'now -1 day' );
	
}

function getRequestLog() {
	
	return @json_decode(@file_get_contents('requests.json'), true);
	
}

function init( $imdb_id ) {
	
	$errorMessage = validateRequest( $imdb_id );
	respond( $errorMessage );

	$xmlData = fetchXml( $imdb_id );
	$links = getPackageLinks( $xmlData );
	$type = getProductionType( $xmlData );
	
	if ( !empty( $links ) ) {
		
		saveDownloadInfoToLog( $imdb_id );
	
	}

	foreach( $links as $link ) {

		$package = downloadPackage( $link, $imdb_id );
		uploadSubtitlesInPackage( $package, $type, $imdb_id ); 

	}
	
}

$imdb_id = $_REQUEST['imdb_id'];

init( $imdb_id );
?>
