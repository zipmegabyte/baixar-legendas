<?php

	function init( $server ) {
		
		$url = $server['REQUEST_URI'];
		$method = $server['REQUEST_METHOD'];
		
		if ( $method == 'GET' ) return checkForOriginal( $url );	
		if ( $method == 'PUT' ) return putNewImage( $url );
		
	}
	
	function checkForOriginal( $url ) {
		
		$path = getPath( $url );
		
		$pathInfo = getPathInfo( $path );
		
		$type = $pathInfo[1];
		$dimension = $pathInfo[2];
		$file = $pathInfo[3];
		
		$original = "$type/original/$file";

		if ( file_exists( $original ) ) {
			buildImageFromOriginal( $path, $dimension, $original, $type );
		}
		else {
			redirectToApplication( $url );
		}
		
	}
	
	function redirectToApplication( $url ) {
		
		if ( preg_match( '/\.jpg$/i', $url ) ) header("Location: http://www.baixarlegendas.com.br/media$url");
		die();
		
	}
	
	function putNewImage( $url ) {
		
		$parameters = getParameters();
		$path = getPath( $url );
		
		$remoteUrl = $parameters['remote_url'];
		$pathInfo = getPathInfo( $path );
		
		validateRequest( $remoteUrl, $pathInfo );
		
		$type = $pathInfo[1];
		$dimension = $pathInfo[2];
		$file = $pathInfo[3];
		
		$original = "$type/original/$file";
		
		saveOriginalImage( $remoteUrl, $original );
		
		if ( $dimension != 'original') buildImageFromOriginal( $path, $dimension, $original, $type );
	}
	
	function buildImageFromOriginal( $path, $dimension, $original, $type ) {
		
		resizeOriginalImage( $path, $dimension, $original, $type );
		
		outputImage( $path );
		
	}
	
	function getParameters() {
		
		parse_str( file_get_contents( 'php://input' ), $post_vars );
		
		return $post_vars;
		
	}
	
	function getPath( $url ) {
		
		$path = parse_url( $_SERVER['REQUEST_URI'] );
		
		return ".{$path['path']}";
		
	}
	
	function getPathInfo( $path ) {
		
		preg_match ( '/^\.\/(filme|serie|episodio)\/(160x240|215|770x370|338x190|original)\/([^\/]+.jpg)$/', $path, $pathInfo );
		
		return $pathInfo;
		
	}
	
	function createPath( $path ) {
		
		 @mkdir( dirname( $path ), 0777, true );
		
	}
	
	function validateRequest( $remoteUrl, $pathInfo ) {
		
		if ( $pathInfo && $remoteUrl ) return true;
			
		header( 'HTTP/1.0 404 Not Found' );
		die();
		
	}
	
	function validateFileExistence( $path ) {
		
		if ( file_exists( $path ) ) return true;
		
		header( 'HTTP/1.0 500 Internal Server Error' );
		die();
		
	}
	
	function saveOriginalImage( $remoteUrl, $path ) {
		
		if ( file_exists( $path ) ) return true;
		
		saveImage( $path, $remoteUrl );

	}
	
	function saveImage( $path, $remoteUrl ) {
		
		createPath( $path );
		
		file_put_contents( $path, fopen( $remoteUrl, 'r' ) );
		
		if ( !filesize( $path ) ) unlink( $path );
		
		validateFileExistence( $path );
		
	}
	
	function resizeOriginalImage( $path, $dimension, $original, $type ) {
		
		switch( $dimension ) {
		
		case 'original':
			return true;
		break;
		
		case '160x240':
			return resizePoster( $path, $original, $dimension );
		break;
		
		case '215':
			return resizePoster( $path, $original, $dimension );
		break;
		
		case '770x370':
			return resizeBackdrop( $path, $original, $dimension, $type == 'episodio' );
		break;
		
		case '338x190':
			return resizeBackdrop( $path, $original, $dimension );
		break;
			
		}
		
		return false;
		
	}
	
	function resizePoster( $path, $original, $dimension ) {
		
		$options = "-background white -gravity center -extent $dimension";
		
		saveThumbnail( $path, $original, $dimension, $options );
		
	}
	
	function resizeBackdrop( $path, $original, $dimension ) {
		
		$options = "-extent $dimension";
		
		saveThumbnail( $path, $original, "$dimension^", $options );
		
	}
	
	function getImageMagickPath() {
		
		//production
		if ( file_exists( '/usr/bin/convert' ) ) return '/usr/bin/convert';
		
		//development
		return '/opt/ImageMagick/bin/convert';
		
	}
	
	function saveThumbnail( $path, $original, $dimension, $options = '' ) {
		
		$convert = getImageMagickPath();
		
		createPath( $path );
		
		exec( "$convert -strip -resize $dimension $options -interlace Plane -quality 75% $original $path" );
		exec( "python ~/smush.py/smush/smush.py -s $path" );
		
		return true;
		
	}
	
	function outputImage( $path ) {
		
		header('Content-Type: image/jpeg');
		readfile( $path );
		
	}

	init( $_SERVER );
?>
