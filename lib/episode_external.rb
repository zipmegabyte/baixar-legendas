module EpisodeExternal

	@@tvdb = TvdbParty::Search.new( Rails.application.config.thetvdb_api_key, 'pt' )

	# This class should be included by the Episode Model.
	# It does all the heavy lifting of fetching data from 
	# external  sources (say, thetvdb.com) and adding it
	# to the Episode. It will use the most accurate method
	# to search for this data.
	# No params are required as it will use the object 
	# own properties for that.
	# It will insert/update related classes like media 
	# entries and episodes.
	#
	# Author:: Leonardo Ribeiro (mailto:leonardotsr@gmail.com)
	# License:: Creative Commons Attribution-ShareAlike 3.0
	
	def fetch_external_data
	
		# The preferred search terms order is by:
		# 1. thetvdb_id
		# 2. imdb_id
		# 3. name and year
		# 4. name
		#
		# When searching by name and year, or only by name
		# we can have multiple results. The match has to be
		# exactly the name provided (case ignored). In case
		# we have a match for the name and not for the year,
		# we'll accept a one-year difference (in both 
		# directions). If no year is provided, we'll accept
		# the more recent result.
		
		if !Rails.application.config.use_thetvdb
		
			logger.info "Not fetching data because 'use_thetvdb' flag is set to false.\n#{self.inspect}"
			return false
			
		end
		
		if self.season.blank?
		
			logger.error "Cannot fetch external data for episode without season.\n#{self.inspect}"
			return false
		
		end
		
		if self.number.blank?
		
			logger.error "Cannot fetch external data for episode without number.\n#{self.inspect}"
			return false
		
		end
		
		if !can_fetch_external_data?
			
			logger.info "Too early to fetch data again for Episode #{self.tv_show.name} #{self.hierarchy}. Last time was #{self.updated_at}."
			return false
			
		end
		
		logger.info "Fetching data for #{self.inspect}"
		
		thetvdb_tv_show = self.tv_show.retrieve_external_object
		
		if thetvdb_tv_show.blank?
		
			logger.error "Could not retrieve data for #{self.tv_show.inspect}"
			return false
		
		end
		
		thetvdb_episode = thetvdb_tv_show.get_episode( self.season, self.number )
		
		if thetvdb_episode.blank?
		
			logger.error "Could not retrieve data for #{self.inspect}"
			return false
		
		end
		
		logger.debug "\nEpisode fetched!\n#{thetvdb_episode.inspect}\n"
		
		self.update_with_external_data( thetvdb_episode )
	
	end
	
	def can_fetch_external_data?
	 return self.updated_at.blank? || !(self.thetvdb_id and self.updated_at > Rails.application.config.external_data_fetch_limit.seconds.ago)
	end
	
	def update_with_external_data( tvdb_episode )
	
		self.thumb = tvdb_episode.thumb
		self.description = tvdb_episode.overview
		self.name = tvdb_episode.name
		self.thetvdb_id = tvdb_episode.id
		self.release_date = tvdb_episode.air_date if !tvdb_episode.air_date.nil?
		self.imdb_id = tvdb_episode.imdb_id if tvdb_episode.imdb_id
		
		self.save
	
	end
	
	def complete?
		return !self.thetvdb_id.blank?
	end
	
end