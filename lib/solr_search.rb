# This program sorts out parameters to build a search query for Solr

class SolrSearch

	#
	# Class Methods
	#
	
	# Public search method
	def self.search( params )
	
		searchable_types = {
			'movie'   => Movie,
			'tv_show' => TvShow
		}
		
		types = searchable_types[ params[:type].try( :downcase ) ] || searchable_types.values
		types = [types] unless types.class.name == 'Array'
		
		q = ( params[:name] || params[:q] || '' ).strip.gsub( /"/, '\"' )
				
		# is it an imdb_id?
		return SolrSearch.search_by_imdb_id( q.downcase, types ) if is_imdb_id?( q )
		return SolrSearch.search_by_imdb_id( params[:imdb_id].downcase, types ) if !params[:imdb_id].blank?
		
		q = q.sub( /\.(?:srt|avi|divx|xvid|mp4|m4v|mpe?g|rmvb)$/i, '' )
		# is it a release?
		return SolrSearch.search_by_name( '"' + q.downcase + '"', Release ) if is_release?( q )
		
		season_number = episode_number = nil
		
		if types.include?( TvShow )
			
			/(.+)\s(?:s|season|temporada|temp)\s*(\d+)(?:\s*(?:ep?|episode|episodio|episódio)\s*(\d+))?(.*)/i.match( q ) {|m|
				q = "#{m[1]} #{m[4]}".strip
				
				season_number = m[2].to_i unless m[2].blank?
				episode_number = m[3].to_i unless m[3].blank?
			}
			
			# is it specific Episode stuff?
			if !season_number.blank? && !episode_number.blank?
				# Exact match
				results = SolrSearch.search_by_name( '"' + q + '"', TvShow )
				
				unless results[:tv_shows].blank?

					tv_show = results[:tv_shows][0]
					episode = Episode.new()
					episode.season = season_number
					episode.number = episode_number
					
					episode.tv_show = tv_show
					
					results[:episodes] << episode
				
				end
				
				return results;
			end #!season_number.blank? && !episode_number.blank?
							
		end #if types.include( Episode ) || types.include?( TvShow )

		return SolrSearch.search_by_name( q, types, params )
	
	end
	
	#
	# Private methods from here on
	#
	private
	
	def self.search_by_name( q, types, params = {} )
	
		search = Sunspot.search types do
			
			if params[:best_match]
			
				year = params[:year].to_i
				with :year, (year - 1)..(year + 1) if year > 0
				
				q = %{#{q}}
				
			end
			
			fulltext q
		
		end
		
		hits = search.hits.to_a
		
		if params[:best_match]

			
			hits.select {|hit|

				SolrSearch.suitable_name?( q, [hit.stored( :name ), hit.stored( :original_name )] )
				
			}
			
		end
		
		return SolrSearch.build_result_set( hits )
		
	end
	
	def self.search_by_imdb_id( q, types )
		
		search = Sunspot.search types do
		
			with :imdb_id, q.downcase
		
		end
		
		return SolrSearch.build_result_set( search.hits )
		
	end
	
	#
	# Build classes from search hits
	#
	def self.build_result_set( hits )
	
		results = {:movies => [], :tv_shows => [], :episodes => [], :releases => []}
	
		hits.each do |hit|

			case hit.class_name
				when 'TvShow'  then results[:tv_shows] << SolrSearch.build( hit )
				when 'Movie'   then results[:movies]   << SolrSearch.build( hit )
				when 'Release' then results[:releases] << SolrSearch.build( hit ) if hit.stored( :path ) != nil
			end
		
		end

		return results
	
	end
	
	#
	# Entity specific builder
	#
	
	def self.build hit
	
		case hit.class_name
		
			when 'Movie'
				return SolrSearch.build_movie( hit )
				
			when 'TvShow'
				return SolrSearch.build_tv_show( hit )
				
			when 'Release'
				return SolrSearch.build_release( hit )
			
		end
	
	end
	
	def self.build_tv_show hit
	
		poster_id = hit.stored( :media_id )
		
		tv_show = TvShow.new
		tv_show.name = hit.stored( :name )
		tv_show.slug = hit.stored( :slug )
		tv_show.year = hit.stored( :year )
		tv_show.created_at = hit.stored( :created_at )
		tv_show.latest_episode = hit.stored( :latest_episode )
		
		if poster_id.blank?
			tv_show_medias = []
		else
			poster = TvShowMedia.new
			poster.media_type = 'poster'
			poster.id = poster_id
			poster.tv_show = tv_show
			tv_show.medias << poster
		end
		
		tv_show.id = hit.primary_key.to_i

		return tv_show
	
	end
	
	def self.build_movie hit
	
		poster_id = hit.stored( :media_id )
		
		movie = Movie.new
		movie.name = hit.stored( :name )
		movie.slug = hit.stored( :slug )
		movie.year = hit.stored( :year )
		movie.created_at = hit.stored( :created_at )
		
		if poster_id.blank?
			movie.medias = []
		else
			poster = MovieMedia.new
			poster.media_type = 'poster'
			poster.id = poster_id
			poster.movie = movie
			movie.medias << poster
		end
		
		movie.id = hit.primary_key.to_i
		
		return movie
	
	end
	
	def self.build_release hit
		return {:url => hit.stored( :path )}
	end
	
	#
	# Test if querystring is an imdb_id
	#
	def self.is_imdb_id? q
	
		q.match /^tt\d+$/i
	
	end
	
	#
	# Test if querystring is a release
	#
	def self.is_release? q
	
		q.match /\W(?:cam|telesync|telecine|screener|r5|dvdscr|dvdrip|vhsrip|tvrip|divx|xvid|x264|1080p|1080i|720p|720i|480p|web-dl|hdtv|proper|h\.264)\W/i
	
	end
	
	def self.suitable_name?( name, names )
		
		suitable = false
		
		if name.blank? or names.join.blank?
			return suitable
		end
		
		name = SolrSearch.normalize_text( name )
		
		names.each {|n|
			
			n = SolrSearch.normalize_text( n )
			
			if n == name
				suitable = true
				
				break
			end
			
		}

		return suitable
		
	end
	
	def self.normalize_text( name )
	
		return '' if name.blank?
	
		return name.gsub( /&/, 'and' ).gsub( /[^\w\d\s]/, '' ).parameterize
	
	end

end