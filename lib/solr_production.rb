#encoding utf-8
module SolrProduction

	def self.included(base)

		define_singleton_method(:solr_search) { :search }

		base.searchable do

			text :name, :stored => true
			text :original_name, :stored => true

			integer( :media_id, :as => 'media_id_stored_integer', :stored => true ) { |e| e.random_poster.try( :id ) }
			string( :slug, :as => 'slug_stored_string', :stored => true )
			string :imdb_id

			time :created_at

			integer :year, :stored => true

			if base.name == 'TvShow'
				string( :latest_episode, :as => 'latest_episode_stored_string', :stored => true )
			end

		end

		def base.search( params )

			result = SolrSearch.search( params )

			if self.ancestors[0].name == 'Movie'
				result = result[:movies]
			elsif self.ancestors[0].name == 'TvShow'
				result = result[:tv_shows]
			end

			return result

		end

		def base.search_all( page = 1, per_page = 30 )

			search = Sunspot.search( self ) {

				order_by :created_at, :desc
				paginate :page => page, :per_page => per_page

			}

			return self.prepare_results( search )

		end

		def base.search_by_imdb_id( imdb_ids )

			search = Sunspot.search( self ) {

				with( :imdb_id, imdb_ids )
				order_by :created_at, :desc

			}

			return self.prepare_results( search )

		end

		def base.prepare_results( search )

			results = WillPaginate::Collection.create( search.hits.current_page, search.hits.per_page, search.total) {|pager|
				pager.replace( search.hits.to_a.map {|hit|

					SolrSearch.build( hit )

				} )
			}

			return results

		end

    end

end
