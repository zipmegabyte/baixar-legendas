module SubtitleFetcher

	def fetch_subtitles

		return if !self.imdb_id

		require 'libarchive_ruby'
		require 'subtitle_util'

		xml_url = "http://www.opensubtitles.org/en/search/sublanguageid-pob/imdbid-#{self.imdb_id}/xml"

		logger.info "Fetching subtitles from #{xml_url}."

		xml = Nokogiri::XML( open( xml_url ) )

		xml.xpath( '/opensubtitles/search/results/subtitle/IDSubtitle' ).each {|subtitle_xml|

			response = Net::HTTP.get( subtitle_xml.attributes['LinkDownload'].value, {"Cookie"=>"user=baixarlegendas-site; remember_sid=sac1ensi4tk0rk5v088ths3dr2;"} )

			next unless response.is_a?(Net::HTTPSuccess)

			original_filename = response.response['Content-Disposition'].match(/filename="(.+)"/)[1]
			filename = Rails.root.join( 'tmp', 'uploads', original_filename )

			open(filename, 'wb') do |file|
				file.write(response.body)
			end

			begin
				Archive.read_open_filename( filename.to_s ) {|a|

					while entry = a.next_header

						next unless File.extname( entry.pathname ) == '.srt'

						name = File.basename( entry.pathname, '.srt' )
						text = a.read_data


						sub = SRT::File.parse( text )
						if !sub.errors.empty? || sub.lines.empty?

							logger.warn "Unable to parse subtitle #{name}"
							next

						end

						release = self.releases.find_or_initialize_by( name: name )
						# make sure it's there
						release.owner = self

						subtitle = release.subtitle
						if subtitle.nil?

							subtitle = Subtitle.new()
							subtitle.text = text
							subtitle.release = release
							release.subtitle = subtitle

							subtitle.save

							logger.info "New subtitle added: #{release.name}"

						end

					end

				}

			ensure

				File.delete( filename )

			end

		}

	end

end
