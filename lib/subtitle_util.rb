class SubtitleUtil

	def self.save_subtitle( params, media = nil, filename = nil )

		subtitle_text = nil
		subtitle_text = params[:subtitle][:text] if !params[:subtitle].blank? && !params[:subtitle][:text].blank?
		result = {
			:success => true
		}

		if ( !params[:file].blank? )

			filename = Rails.root.join( 'tmp', 'uploads', File.basename( params[:file] ) ) if filename.blank?

			if File.exists?( filename )
				subtitle_text = File.read( filename )
				File.unlink( filename )
			end

		end

		if !subtitle_text then

			Rails.logger.error "Unable to fetch text for subtitle in #{filename}."
			result = {
				:success => false,
				:error => "Um erro interno impediu o upload da legenda #{filename}"
			}
			return result

		end

		if media.nil? then
			if params[:type] == 'movie'
				media = Movie.from_uploaded_data( params[:media] )
				#media.save! if media.new_record?

			elsif params[:type] == 'tv_show'
				tv_show = TvShow.from_uploaded_data( params[:media] )
				#tv_show.save! if tv_show.new_record?

				if  tv_show.id.blank? || tv_show.id == 0
					media = Episode.new()
					media.number = params[:episode][:number]
					media.season = params[:episode][:season]
					media.tv_show = tv_show
				else

					media = Episode.find_or_initialize_by(
						number: params[:episode][:number],
						season: params[:episode][:season],
						tv_show_id: tv_show.id
					)

				end

			elsif params[:type] == 'episode'

				media = Episode.where( params[:media] ).first

			end

		end

		raise ActiveRecord::RecordNotFound if media.blank?

		params[:release][:name].split(/\n/).uniq.each {|name|
			next if name.strip.blank?

			# Workaround for medias coming from Solr search
			if media.new_record?

				if !media.id.blank?
					media = media.class.find( media.id )
				elsif media.class.name == 'Episode'
					media = Episode.find_by_season_and_number_and_tv_show_id( media.season, media.number, media.tv_show.id ) || media
				end

			end

			release = media.releases.find_by_slug( name.parameterize )
			if release.blank?
				release = Release.new()
				release.name = name

				media.releases << release
			end
			# make sure it's there
			release.owner = media

			subtitle = release.subtitle
			if subtitle.nil?

				subtitle = Subtitle.new()
				subtitle.description = params[:subtitle][:description] if params[:subtitle]
				subtitle.release = release

			end

			subtitle.text = subtitle_text

			subtitle.save


			(result[:subtitles] ||= []) << subtitle

		}

		return result

	end

	def self.validate_subtitle( file )

		if file.blank?

			return 400 # Bad Request

		elsif file.size > 1048576

			return 413 # Request Entity Too Large

		else

			sub = SRT::File.parse( File.new( file ) )

			if !sub.errors.empty? || sub.lines.empty?

				return 415 # Unsupported Media Type

			end

		end

		return 200 # OK

	end

	def self.parse_subtitle_name( name )

		data = {}

		# It's a TvShow
		name.match( Rails.application.config.regexp_tv_show ) {|match|

			data = {

				:type => 'tv_show',
				:release => {
					:name => match[1]
				},
				:media => {
					:name => match[2].gsub( /[._]+/, ' ' ),
					:year => match[3]
				},
				:episode => {
					:season => match[4].to_i + match[6].to_i + match[8].to_i, #the nil fields become 0, sum is ok.
					:number => match[5].to_i + match[7].to_i + match[9].to_i
				},
				:best_match => true

			}

		}

		# It's a Movie
		name.match( Rails.application.config.regexp_movie ) {|match|

			data = {

				:type => 'movie',
				:release => {
					:name => match[1]
				},
				:media => {
					:name => match[2].gsub( /\./, ' ' ).strip,
					:year => match[3]
				},
				:best_match => true

			}

		} if data.empty?

		return data

	end

	def self.get_subtitle_media( data, fixed_data = {} )

		return if data.blank?

		media = nil

		search_data = {
			:type => data[:type],
			:release => data[:release][:name],
			:name => data[:media][:name],
			:year => data[:media][:year],
			:best_match => data[:best_match]
		}

		# It's a TvShow - Media is episode
		if data[:type] == 'tv_show'

			# Prevent saving garbage
			if data[:episode][:season] != 0 && !data[:release][:name].blank?

				tv_show = TvShow.search( search_data ).first

				if tv_show.nil?
					tv_show = TvShow.new()
					tv_show.name = search_data[:name]
					tv_show.year = search_data[:year]
				end

				media = nil

				if tv_show.id

					media = Episode.where(
						:season => data[:episode][:season],
						:number => data[:episode][:number],
						:tv_show => tv_show
					).first

				end

				unless media

					media = Episode.new()
					media.season = data[:episode][:season]
					media.number = data[:episode][:number]
					media.tv_show = tv_show

				end

				# new show, get full data
				if tv_show.id.blank?

					Rails.logger.info "Retrieving full data for #{data}"
					tv_show.fetch_external_data

					media.tv_show = tv_show

					# only save if found
					media = nil unless tv_show.complete?

				#new episode, get only episode data
				elsif media.new_record?

					media.save

				end

			end #if data[:season] != 0 and !data[:name].blank?

		# It's a Movie
		elsif data[:type] == 'movie'

			media = Movie.search( search_data ).first

			if media.nil?
					media = Movie.new()
					media.name = search_data[:name]
					media.year = search_data[:year]
				end

			unless media.complete?

				media.fetch_external_data_fast

				if media.id.blank?
					media = nil
				end

			end

		end

		return media

	end

	def self.sanitize( subtitle )

		subtitle = subtitle.force_encoding( 'ISO-8859-1' ).encode( 'UTF-8' ) if !subtitle.valid_encoding?
		blacklist = [
			/open\s?subtitles/i,
			/tvsubtitles\.net/i
		]

		srt = SRT::File.parse( subtitle )
		srt.lines.reject! {|l|

			l.text.blank? || blacklist.any? {|i| l.text.join.match i}

		}
		srt.lines.each_with_index {|l, i| l.sequence = i + 1}

		return srt.to_s

	end

end
