# encoding: utf-8
module FulltextSearch
	
	module ClassMethods
	
		def full_text_search( data )
			
			@searchable = [:name] if @searchable.nil?
			@order = :year if @order.nil?
			
			return if @searchable.empty?
			
			query_string = ( data[:name] || data[:q] || '' ).strip
			found = []
			
			return found if ( query_string.empty? )
			
			# 4 is the min length for fulltext search, do a normal search
			if ( query_string.length < 4 )
				qs_for_like = query_string.gsub(/[!%_]/) { |x| '!' + x }
				found = self.where( "CONCAT_WS(' ', " + @searchable.join(', ') + ") LIKE ? ESCAPE '!'", "%#{qs_for_like}%" ).order( 'year DESC' )
				found.where( :year => data[:year] ) if ( data[:year] && !data[:year].strip!.empty? )
			else
				# prepare keyword for fulltext search (ugly hack)
				fulltext_q = ( sanitize query_string )[1..-2]
				
				if ( data[:imdb_id] )
					found = self.find_by_imdb_id( data[:imdb_id] )					
				else
					found = self.where( "MATCH(" + @searchable.join(', ') + ") AGAINST('#{fulltext_q}')" )
					found.where( :year => data[:year] ) unless data[:year].blank?
					
					# sometimes it ignores entries.
					if found.empty?
											
						found = self.where( @searchable.map {|s| "#{s} LIKE \"#{fulltext_q}\""}.join(' OR ') )
						
					end
				end
				
			end

			found
		end
		
		def search( params )

			search_params = params.reject {| k, v | v.blank?}
			search_params.reject! {|k, v| k.to_s == 'year'} if params[:best_match]
			result = nil
			
			if !params[:imdb_id].blank?
				
				result = self.find_all_by_imdb_id( params[:imdb_id] )
									
			else
			
				result = self.full_text_search( search_params ).try( :paginate, { :page => params[:page] } )

				result = result.select {|media| media.suitable_name?( params[:name] ) and media.suitable_year?( params[:year] )}.sort{|a, b| ( b.year || 0 ) <=> ( a.year || 0 )} unless params[:best_match].blank?
								
			end
			
			return result
		
		end
		
	end
	
	def self.included(base)
    	base.extend(ClassMethods)
    end
    
    def suitable_name?( name )
		
		if name.blank? or ( self.name.blank? and self.original_name.blank? )
			return false
		end
		
		name = normalize_text( name )
		self.name = normalize_text( self.name )
		self.original_name = normalize_text( self.original_name )
		
		return ((self.name == name) or (self.original_name == name))
		
	end
	
	def normalize_text( name )
	
		return '' if name.blank?
	
		return name.gsub( /&/, 'and' ).gsub( /[^\w\d\s]/, '' ).parameterize
	
	end
	
	def suitable_year?( year )
	
		return true if self.year.blank? || year.blank?
		
		return (self.year - year.to_i).abs <= 1
	
	end

end