class Cloudflare

	def self.purge( path = ['', '/'] ) #purges home if no path is passed
	
		cf = CloudFlare::connection( Rails.application.config.cloudflare_api_key, Rails.application.config.cloudflare_email )
		
		if path.kind_of?( Array )
			path.each {|p| Cloudflare.purge_url( cf, p )}
		else
			output = Cloudflare.purge_url( cf, path )
		end

	end
	
	private
	def self.purge_url( cf, path )
	
		begin
			url = "http://#{Rails.application.config.domain}#{path}"
			cf.zone_file_purge( Rails.application.config.cloudflare_domain, url )
		rescue Exception => e  

			Rails.logger.error "Error purging CloudFlare cache for #{url}"
			Rails.logger.error e.message
			Rails.logger.error e.backtrace.inspect

		end
	
	end

end