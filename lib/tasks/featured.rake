namespace :featured do
  desc "Update settings with featured Movies and TvShows"
  task :update => :environment do
  	
  	tv_shows = ActiveRecord::Base.connection.exec_query(
	  	"SELECT tv_show.imdb_id
		FROM impression
		INNER JOIN subtitle ON subtitle.id = impressionable_id AND impressionable_type = 'Subtitle'
		INNER JOIN `release` ON release.id = subtitle.release_id
		INNER JOIN episode ON release.owner_id = episode.id AND release.owner_type = 'Episode'
		INNER JOIN tv_show ON tv_show.id = episode.tv_show_id		
		#AND episode.release_date > NOW() - INTERVAL 8 DAY
		AND impression.referrer IS NOT NULL
		AND impression.updated_at > NOW() - INTERVAL 1 WEEK
		GROUP BY 1
		ORDER BY episode.release_date DESC, COUNT(*) DESC
		LIMIT #{Rails.application.config.show_featured_tv_shows}"
	).rows
	
	Settings.featured_tv_shows = tv_shows.try( :to_a ).try( :flatten ) || []
	
	movies = ActiveRecord::Base.connection.execute(
	  	"SELECT movie.imdb_id
		FROM impression
		INNER JOIN subtitle ON subtitle.id = impressionable_id AND impressionable_type = 'Subtitle'
		INNER JOIN `release` ON release.id = subtitle.release_id
		INNER JOIN movie ON release.owner_id = movie.id AND release.owner_type = 'Movie'
		#AND movie.release_date > NOW() - INTERVAL 1 YEAR
		AND impression.referrer IS NOT NULL
		AND impression.updated_at > NOW() - INTERVAL 1 WEEK
		GROUP BY 1
		ORDER BY movie.release_date DESC, COUNT(*) DESC
		LIMIT #{Rails.application.config.show_featured_movies}"
	)
	
	Settings.featured_movies = movies.try( :to_a ).try( :flatten ) || []
	
	
	movies = Movie.includes( :medias ).where( :imdb_id => Settings.featured_movies )
	tv_shows = TvShow.includes( :medias ).where( :imdb_id => Settings.featured_tv_shows )
  	
	# Update latest_episode
	tv_shows.each {|tv_show| tv_show.update_latest_episode }
  	
	(movies + tv_shows).each {|item|
	  	
		item.fetch_external_data if item.medias.blank?
		
		item.index
	  	
	}
  end

end
