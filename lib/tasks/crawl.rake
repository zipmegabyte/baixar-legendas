namespace :crawl do

	require 'open-uri'
	require 'archive'

 	desc 'Crawl pages for subtitles'

 	task :legendas => :environment do

 		begin

			@url = nil
 			new_crawl_date = DateTime.now
 			crawl( new_crawl_date )

 		rescue Exception => e

 			errors = [e.message + " (#{@url})", e.backtrace.inspect]
 			send_errors( errors, new_crawl_date )

 		end

 	end

 	def crawl( new_crawl_date )

	 	errors = []
	 	successes = []
	 	title = nil
	 	found_older = false
	 	page = 1
	 	last_crawl_date = Settings.last_crawl_date || 1.month.ago

		Rails.logger.info "\nCrawling legendas.tv subtitles published after #{last_crawl_date.strftime('%d/%m/%Y %Hh%M')}.\n"

	 	until found_older

		 	doc = Nokogiri::HTML( open( "http://legendas.tv/util/carrega_destaques/todos/#{page}", 'User-Agent' => 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36' ) )
		 	page = page + 1

		 	doc.css('div.film').each do |film|
		 		link = film.css('h3 a')[0]
		 		posted_at = Time.parse( film.css('p')[2].content )
		 		title = link.children[0].content
		 		tv_show = link.css('span')[1].try(:text)
		 		@url = link['href']

		 		Rails.logger.debug "#{title}: #{posted_at.strftime('%d/%m/%Y %Hh%M')} (#{posted_at > last_crawl_date})"

		 		if posted_at < last_crawl_date

		 			found_older = true
		 			next

		 		end

		 		Rails.logger.info "\n"

		 		hash = link['href'].match(/^\/download\/(\w+)\/.+$/)[1]
		 		url = "http://legendas.tv/pages/downloadarquivo/#{hash}"

		 		Rails.logger.info "Downloading #{url}."
		 		archive = download( url )
		 		if archive.blank?
			 		errors << "Unable to download archive for #{title} at #{url}."
			 		next
			 	end

			 	Rails.logger.info "Extracting #{archive}."
			 	files = extract( archive )
			 	if files.blank?
			 		errors << "Unable to extract archive for #{title} at #{url}."
			 		next
			 	end

			 	files.each {|file|

				 	basename = File.basename( file ).force_encoding('iso-8859-1').encode('utf-8')

				 	File.open( file, 'rb' ) {|f|

						status = SubtitleUtil.validate_subtitle( f )
						if status != 200

							errors << "Invalid subtitle #{basename} for #{title} at #{url}: status #{status}."
							next

						end

				 	}

				 	data = SubtitleUtil.parse_subtitle_name( basename )
				 	if data.blank?

				 		Rails.logger.info "Could not parse data from subtitle #{basename} for #{title} at #{url} . Trying from website data."

				 		# dummy data structure
				 		data = {

				 			:type => nil,
							:release => {
								:name => nil
							},
							:media => {
								:name => nil,
								:year => nil
							},
							:episode => {
								:season => nil,
								:number => nil
							},
							:best_match => true

				 		}

					end


					# Transform media data based on info on page
					data[:media][:name] = title

					if tv_show.blank?
						data[:type] = 'movie'
					else

						tv_show.match( /s(\d\d)e(\d\d)/i ) {|match|

							data[:type] = 'tv_show'
							data[:episode] = {} if data[:episode].blank?
							data[:episode][:season] = match[1].to_i if data[:episode][:season].blank?
							data[:episode][:number] = match[2].to_i if data[:episode][:number].blank?

						}

					end

					if data[:release][:name].blank?

						data[:release][:name] = File.basename( basename, '.srt' ).force_encoding('iso-8859-1').encode('utf-8')

					end

				 	media = SubtitleUtil.get_subtitle_media( data )
				 	if media.blank?

						errors << "Could not find corresponding media for subtitle #{basename.force_encoding("UTF-8")} for #{title.force_encoding("UTF-8")} at #{url} ."
						next

				 	end

					subtitle_text = File.read( file )
					data[:subtitle] = {:text => subtitle_text}

					Rails.logger.info "Saving #{data[:release][:name]}."
					# Try to convert to UTF-8 only if needed. Very costly.
					begin

				 		obj = SubtitleUtil.save_subtitle( data, media, file )

				 	rescue

				 		data[:subtitle][:text] = data[:subtitle][:text].force_encoding('iso-8859-1').encode('utf-8')
				 		obj = SubtitleUtil.save_subtitle( data, media, file )

				 	end

				 	delete_file file

					if !obj[:success]

						errors << "Could not save subtitle #{basename} for #{title} at #{url} .\n"

					else

						successes << "#{basename} for #{title}."

					end

			 	}

		 	end #doc.css('div.film').each

		 end #until found_older

	 	send_errors( errors, new_crawl_date )

	 	send_successes( successes, new_crawl_date )

	 	Settings.last_crawl_date = new_crawl_date

 	end

 	def send_errors( errors, new_crawl_date )

 		if !errors.blank?

	 		Rails.logger.error "\n"
	 		Rails.logger.error "------ ERRORS ------"

	 		errors.each {|error| Rails.logger.error error}

	 		Rails.logger.error "--------------------"

	 		LoggerMailer.errors( errors, new_crawl_date ).deliver

	 	end

 	end

 	def send_successes( successes, new_crawl_date )

 		if !successes.blank?

	 		LoggerMailer.successes( successes, new_crawl_date ).deliver

	 	end

 	end

 	def download( url )

 		response = Typhoeus.get(url, followlocation: true)


 		if response.success?

 			filename = response.effective_url
 			filename = Rails.root.join( 'tmp', 'uploads', File.basename( filename ).force_encoding('iso-8859-1').encode('utf-8') )

 			File.open( filename, 'wb' ) {|f| f.write( response.body ) }

 			return filename.to_s

 		end

 	end

 	def extract( archive )

 		subtitles = []

 		a = Archive.new( archive )

 		Dir.chdir( Rails.root.join( 'tmp', 'uploads' ) )
 		subtitles = a.extract_if {|entry| File.extname( entry.path.downcase ) == '.srt'}

 		delete_file archive

 		return subtitles
 	end

 	def delete_file( file )

 		begin
 			File.unlink file
 		rescue
 		end

 	end

end
