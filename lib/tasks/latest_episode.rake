namespace :latest_episode do
  desc "Updates all TvShows to the latest episode with subtitles"
  task :update_all => :environment do
  
  	Episode.joins( :releases ).pluck( :tv_show_id ).uniq.each do |tv_show_id|
  	
  		episode = TvShow.find( tv_show_id ).episodes.joins( :releases ).order( 'release_date DESC' ).limit( 1 ).first
  		
  		episode.tv_show.update_attribute :latest_episode, episode.hierarchy if episode
  	
  	end
  
  end

end
