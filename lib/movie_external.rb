module MovieExternal

	@@fast = false

	# This class should be included by the Movie Model.
	# It does all the heavy lifting of fetching data from
	# external  sources (say, themoviedb.com) and adding it
	# to the Movie. It will use the most accurate method
	# to search for this data.
	# No params are required as it will use the object
	# own properties for that.
	# It will insert/update related classes like media
	# entries.
	#
	# Author:: Leonardo Ribeiro (mailto:leonardotsr@gmail.com)
	# License:: Creative Commons Attribution-ShareAlike 3.0

	def fetch_external_data_fast

		@@fast = true
		return fetch_external_data

	end

	def fetch_external_data

		# The preferred search terms order is by:
		# 1. themoviedb_id
		# 2. imdb_id
		# 3. name and year
		# 4. name
		#
		# When searching by name and year, or only by name
		# we can have multiple results. The match has to be
		# exactly the name provided (case ignored). In case
		# we have a match for the name and not for the year,
		# we'll accept a one-year difference (in both
		# directions). If no year is provided, we'll accept
		# the more recent result.

		if !Rails.application.config.use_themoviedb

			logger.info "Not fetching data because 'use_themoviedb' flag is set to false.\n#{self.inspect}"
			return false

		end

		if !can_fetch_external_data?
			logger.info "Too early to fetch data again for Movie #{self.name}. Last time was #{self.data_fetched_at}."
			return false

		end

		logger.info "Fetching data for #{self.inspect}"

		begin

			return search_by_themoviedb_id if !self.themoviedb_id.blank?
			return search_by_imdb_id if !self.imdb_id.blank?
			return search_by_name if !self.name.blank?

		rescue Exception => e

			logger.error e

			return false

		end

	end

	def can_fetch_external_data?
	 return !(self.data_fetched_at and self.data_fetched_at > Rails.application.config.external_data_fetch_limit.seconds.ago)
	end

	# Cleanest way to fetch Movie metadata
	def search_by_themoviedb_id

		initTmdb

		tmdb_movie = TmdbMovie.find( :id => self.themoviedb_id, :limit => 1, :expand_results => true )

		# No data, go home early
		return false unless tmdb_movie

		return update_object( tmdb_movie )

	end

	# Also nice and clean
	def search_by_imdb_id

		initTmdb

		tmdb_movie = TmdbMovie.find( :imdb => self.imdb_id, :limit => 1, :expand_results => true )

		# No data, go home early
		return false unless tmdb_movie

		return update_object( tmdb_movie )
	end

	# Least reliable way of searching
	def search_by_name( lang = 'pt' )

		initTmdb( lang )

		# Movie name not set
		return false if self.name.nil? or self.name.empty?

		results = TmdbMovie.find( :title => self.name, :limit => 5, :expand_results => false )

		return false if results.blank?

		results = [results] unless results.class.name == 'Array'

		# No results from search
		return false if results.empty?

		matches = []

		self_name = self.name.nil? ? nil : self.name.parameterize
		self_original_name = self.original_name.nil? ? nil : self.original_name.parameterize

		results.each {|result|

			next if result.title.blank? and result.original_title.blank?

			# Do we need to treat year?
			if !self.year.blank? and self.year > 0 and !result.release_date.blank? then

				# The year is different by more than one
				next if (self.year - result.release_date.to_date.year).abs > 1
			end

			# Parameterize is used here to ignore any small discrepancies in names
			result_name = result.title.parameterize
			result_original_name = result.original_title.parameterize

			# It'll be ugly
			next unless
				self_name == result_name or
				self_name == result_original_name or
				self_original_name == result_name or
				self_original_name == result_original_name

			matches << result
		}

		# No result matched the TvShow name reliably, try english
		if matches.empty? && lang == 'pt'
			return self.search_by_name 'en'
		end

		# Results are all acceptable. Use the most recent
		tmdb_movie = matches.sort{|a, b| b.release_date <=> a.release_date}.first

		# Sorry, we need all the data
		self.themoviedb_id = tmdb_movie.id
		self.search_by_themoviedb_id

	end

	def update_object( tmdb_movie )

		self.name            = tmdb_movie.title
		self.original_name   = tmdb_movie.original_title
		self.description     = tmdb_movie.overview
		self.genre           = tmdb_movie.genres.blank? ? nil : tmdb_movie.genres.map{|genre| genre.name}.join(', ')
		self.year            = tmdb_movie.release_date.blank? ? nil : tmdb_movie.release_date.to_date.year
		self.release_date    = tmdb_movie.release_date.blank? ? nil : tmdb_movie.release_date.to_date
		self.tagline         = tmdb_movie.tagline
		self.language        = tmdb_movie.spoken_languages.blank? ? nil : tmdb_movie.spoken_languages.first.iso_639_1
		self.rating          = tmdb_movie.vote_average
		self.status          = tmdb_movie.status
		self.website         = tmdb_movie.homepage
		self.runtime         = tmdb_movie.runtime
		self.imdb_id         = tmdb_movie.imdb_id
		self.themoviedb_id   = tmdb_movie.id
		self.data_fetched_at = DateTime.now

		saved = false

		if self.new_record?

			self.save

		else

			name            = ActiveRecord::Base::sanitize( self.name )
			original_name   = ActiveRecord::Base::sanitize( self.original_name )
			description     = ActiveRecord::Base::sanitize( self.description )
			genre           = ActiveRecord::Base::sanitize( self.genre )
			year            = ActiveRecord::Base::sanitize( self.year )
			release_date    = ActiveRecord::Base::sanitize( self.release_date )
			tagline         = ActiveRecord::Base::sanitize( self.tagline )
			language        = ActiveRecord::Base::sanitize( self.language )
			rating          = ActiveRecord::Base::sanitize( self.rating )
			status          = ActiveRecord::Base::sanitize( self.status )
			website         = ActiveRecord::Base::sanitize( self.website )
			runtime         = ActiveRecord::Base::sanitize( self.runtime )
			imdb_id         = ActiveRecord::Base::sanitize( self.imdb_id )
			themoviedb_id   = ActiveRecord::Base::sanitize( self.themoviedb_id )
			data_fetched_at = ActiveRecord::Base::sanitize( self.data_fetched_at )

			query = "UPDATE movie SET name = #{name}, original_name = #{original_name}, description = #{description}, genre = #{genre}, year = #{year}, release_date = #{release_date}, tagline = #{tagline}, language = #{language}, rating = #{rating}, status = #{status}, website = #{website}, runtime = #{runtime}, imdb_id = #{imdb_id}, themoviedb_id = #{themoviedb_id}, data_fetched_at = #{data_fetched_at} WHERE id = #{self.id}"

			saved = ActiveRecord::Base.connection.execute( query )

		end # if self.new_record?

	    if !@@fast

		  get_media( tmdb_movie )
		  get_series( tmdb_movie )

		end

	  	return saved

	end

	def get_media( tmdb_movie )

		medias = {
			'poster' => tmdb_movie.posters ||= [],
			'backdrop' => tmdb_movie.backdrops ||= []
		}

		data = [];

		medias.each_pair {|type, list|

			data << list.map {|tmdb_media|

				url = ActiveRecord::Base::sanitize( tmdb_media.sizes.original.url )
				media_type = ActiveRecord::Base::sanitize( type )
				language = ActiveRecord::Base::sanitize( tmdb_media.iso_639_1 )
				movie_id = ActiveRecord::Base::sanitize( self.id )

				"(#{movie_id}, #{media_type}, #{url}, MD5(#{url}), #{language})"

			}

		}

		return if data.empty?

		query = "INSERT IGNORE INTO movie_media(movie_id, media_type, url, url_hash, language) VALUES #{data.join( ', ' )}"

		ActiveRecord::Base.connection.execute( query )

	end

	def get_series( tmdb_movie )

		return if tmdb_movie.belongs_to_collection.blank?

		series = Series.find_or_initialize_by( name: tmdb_movie.belongs_to_collection.name )

		# Do I really need this?
		#series.movies << self
		self.series << series

	end

	# has its data been fetched already?
	def complete?
		!self.data_fetched_at.nil?
	end

	# set basic config options
	def initTmdb( lang = 'pt' )
		Tmdb.api_key = Rails.application.config.themoviedb_api_key
		Tmdb.default_language = lang
	end

end
