module TvShowExternal

	@@tvdb = TvdbParty::Search.new( Rails.application.config.thetvdb_api_key, 'pt' )
	@@fast = false

	# This class should be included by the TvShow Model.
	# It does all the heavy lifting of fetching data from 
	# external  sources (say, thetvdb.com) and adding it
	# to the TvShow. It will use the most accurate method
	# to search for this data.
	# No params are required as it will use the object 
	# own properties for that.
	# It will insert/update related classes like media 
	# entries and episodes.
	#
	# Author:: Leonardo Ribeiro (mailto:leonardotsr@gmail.com)
	# License:: Creative Commons Attribution-ShareAlike 3.0
	
	def fetch_external_data_fast

		@@fast = true
		return fetch_external_data
		
	end
	
	def fetch_external_data
	
		# The preferred search terms order is by:
		# 1. thetvdb_id
		# 2. imdb_id
		# 3. name and year
		# 4. name
		#
		# When searching by name and year, or only by name
		# we can have multiple results. The match has to be
		# exactly the name provided (case ignored). In case
		# we have a match for the name and not for the year,
		# we'll accept a one-year difference (in both 
		# directions). If no year is provided, we'll accept
		# the more recent result.
		
		if !Rails.application.config.use_thetvdb
		
			logger.info "Not fetching data because 'use_thetvdb' flag is set to false.\n#{self.inspect}"
			return false
			
		end
		
		if !can_fetch_external_data?		
			logger.info "Too early to fetch data again for TV Show #{self.name}. Last time was #{self.data_fetched_at}."
			return false
			
		end
		
		logger.info "Fetching data for #{self.inspect}"
		
		return search_by_remote_id if ( !self.thetvdb_id.blank? || !self.imdb_id.blank? )
		
		return search_by_name if ( !self.name.nil? and !self.name.empty? )
	
	end
	
	def can_fetch_external_data?
		return !(self.data_fetched_at and self.data_fetched_at > Rails.application.config.external_data_fetch_limit.seconds.ago)
	end
	
	def retrieve_external_object
	
		# do we have thetvdb_id?
		return @@tvdb.get_series_by_id( self.thetvdb_id ) if !self.thetvdb_id.blank?
		
		if !self.imdb_id.blank?
			# trying with imdb_id
			require 'open-uri'
			
			open( "http://thetvdb.com/api/GetSeriesByRemoteID.php?imdbid=#{self.imdb_id}&lang=pt" ) {|f|
		  		
		  		begin
		  		
			  		xml_str = ''  
			  		f.each_line {|l| xml_str << l}
			  		
			  		response = Hash.from_xml( xml_str )
			  		
			  		if response["Data"] && response["Data"]["Series"] then
			  		
			  			tvdb_tv_show = TvdbParty::Series.new( @@tvdb, response['Data']['Series'] )
			  			
			  			return tvdb_tv_show
	
			  		end
			  		
			  		return false
			  		
			  	rescue Exception => e
			  	
			  		logger.error e
			  		
			  		return false
			  	end
			 }
		end #if !self.imdb_id.blank?
		
	end
	
	# Cleanest way to fetch TvShow metadata
	def search_by_remote_id
		
		tvdb_tv_show = retrieve_external_object
		
		# No data, go home early
		return false if tvdb_tv_show.blank?
		
		return update_object( tvdb_tv_show )
		
	end
	
	# Uses a dirty XML fetch hack, since tvdb_party doesn't support imdb_id
	def search_by_imdb_id
		
	end
	
	# Least reliable way of searching
	def search_by_name
	
		# TvShow name not set
		return false if self.name.nil? or self.name.empty?
	
		results = @@tvdb.search( self.name )
		
		# No results from search
		return false if results.nil? or results.empty?
		
		# Too many results, awfully unreliable, maybe an attempt to break things
		return false if results.length > 30
		
		matches = []
		
		results.each {|result|

			# Account for a bug in tvdb_party
			result = TvdbParty::Series.new( @@tvdb, result )

			next if result.name.nil? or result.name.empty?
			
			# Do we need to treat year?
			if !self.year.blank? and self.year > 0 and !result.first_aired.nil? then
			
				# Some tv shows have the year after their names. Let's remove that
				m = /^(.+)( \(#{result.first_aired.year}\))/.match result.name
	
				result.name = m[1] unless m.nil?

				# The year is different by more than one
				next if (self.year - result.first_aired.year).abs > 1
			end
			
			# Parameterize is used here to ignore any small discrepancies in names
			next if self.name.parameterize != result.name.parameterize
			
			matches << result
		}

		# No result matched the TvShow name reliably, save the fetched_at date anyway
		if matches.empty?
			
			self.update_attribute( :data_fetched_at, DateTime.now ) unless self.new_record?
			
			return false
		end
		
		# Results are all acceptable. Use the most recent
		tvdb_tv_show = matches.sort{|a, b| b.first_aired <=> a.first_aired}.first
		
		return update_object( tvdb_tv_show )
	
	end
	
	def update_object( tvdb_tv_show, try_another_entity = true )

		# Fetch original (en) name. It is an extra request, but it'll save a lot
		# of traffic in the long run.
		tvdb_tv_show_en = @@tvdb.get_series_by_id( tvdb_tv_show.id, 'en' )
		#saved = self.id.blank?
		
		self.description     = tvdb_tv_show.overview           unless tvdb_tv_show.overview.blank?
		self.name			 = tvdb_tv_show.name               unless tvdb_tv_show.name.blank?
		self.original_name   = tvdb_tv_show_en.name            unless tvdb_tv_show_en.name.blank?
		self.genre			 = tvdb_tv_show.genres.join( ';' ) unless tvdb_tv_show.genres.blank?
		self.network		 = tvdb_tv_show.network            unless tvdb_tv_show.network.blank?
		self.runtime		 = tvdb_tv_show.runtime            unless tvdb_tv_show.runtime.blank?
		self.content_rating	 = tvdb_tv_show.rating             unless tvdb_tv_show.rating.blank?
		self.airs_time		 = tvdb_tv_show.air_time           unless tvdb_tv_show.air_time.blank?
		self.imdb_id		 = tvdb_tv_show.imdb_id            unless tvdb_tv_show.imdb_id.blank?
		self.thetvdb_id		 = tvdb_tv_show.id                 unless tvdb_tv_show.id.blank? #redundant?
		self.release_date	 = tvdb_tv_show.first_aired        unless tvdb_tv_show.first_aired.blank?
		self.year			 = tvdb_tv_show.first_aired.year   unless tvdb_tv_show.first_aired.blank?
		self.data_fetched_at = DateTime.now
		
		saved = self.save
		
		# there's a chance this Tv Show info refers to another entity. Let's try that.
		if !saved && try_another_entity
		
			tv_show = TvShow.where( 'imdb_id= ? OR thetvdb_id = ?', tvdb_tv_show.imdb_id, tvdb_tv_show.id ).first
			
			if tv_show
	
				self.id = tv_show.id
				self.reload
				
				return self.update_object( tvdb_tv_show, false )
	
			end
			
			# bummer
			return false
		
		end

		if !@@fast

			get_media( tvdb_tv_show )
			get_episodes( tvdb_tv_show )
			
		end

  		return saved
	
	end
	
	def get_media( tvdb_tv_show )

		return if tvdb_tv_show.banners.blank?

		#require 'digest/md5'
		
		data = tvdb_tv_show.banners.map {|tvdb_media|

			url = ActiveRecord::Base::sanitize( "http://thetvdb.com/banners/#{tvdb_media.path}" )
			#url_hash = Digest::MD5.digest( url )
			media_type = nil;
			
			if tvdb_media.banner_type == 'fanart'
			
				media_type = 'backdrop'
				
			else
			
				media_type = tvdb_media.banner_type
				
			end
			
			media_type = ActiveRecord::Base::sanitize( media_type )
			
			#media.media_type2 = tvdb_media.banner_type2
			season = ActiveRecord::Base::sanitize( tvdb_media.season )
			language = ActiveRecord::Base::sanitize( tvdb_media.language )
			tv_show_id = self.id

			"(#{tv_show_id}, #{media_type}, #{season}, #{url}, MD5(#{url}), #{language})"
			
			#media.save
						
		}.join(', ')

		query = "INSERT IGNORE INTO tv_show_media(tv_show_id, media_type, season, url, url_hash, language) VALUES #{data}"
		
		ActiveRecord::Base.connection.execute( query )
		
	end
	
	def get_episodes( tvdb_tv_show )
	
		return if tvdb_tv_show.episodes.blank?
	
		data = tvdb_tv_show.episodes.map {|tvdb_episode|
		
			next unless tvdb_episode.season_number and tvdb_episode.number
		
			season = ActiveRecord::Base::sanitize( tvdb_episode.season_number )
			number = ActiveRecord::Base::sanitize( tvdb_episode.number )
			thumb = ActiveRecord::Base::sanitize( tvdb_episode.thumb )
			description = ActiveRecord::Base::sanitize( tvdb_episode.overview )
			name = ActiveRecord::Base::sanitize( tvdb_episode.name )
			thetvdb_id = ActiveRecord::Base::sanitize( tvdb_episode.id )
			release_date = ActiveRecord::Base::sanitize( tvdb_episode.air_date )
			imdb_id = ActiveRecord::Base::sanitize( tvdb_episode.imdb_id )
			tv_show_id = self.id
			
			"(#{season}, #{number}, #{thumb}, #{description}, #{name}, #{thetvdb_id}, #{release_date}, #{imdb_id}, #{tv_show_id})"
		
		}.join(', ')
		
		return if data.blank?
		
		query = "INSERT IGNORE INTO episode(season, number, thumb, description, name, thetvdb_id, release_date, imdb_id, tv_show_id) VALUES #{data}"
		
		ActiveRecord::Base.connection.execute( query )
	
	end
	
	# has its data been fetched already? 
	def complete?
		!self.data_fetched_at.nil?
	end
	
end