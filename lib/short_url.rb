module ShortUrl

	def short_url
	
		"/#{self.class.name.downcase[0]}#{self.id.to_s(36)}"
	
	end

end