# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130720214707) do

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "episode", :force => true do |t|
    t.string    "name"
    t.text      "description"
    t.integer   "season",                                                   :null => false
    t.integer   "number",                                                   :null => false
    t.integer   "tv_show_id",                                               :null => false
    t.string    "thetvdb_id",   :limit => 31
    t.string    "imdb_id",      :limit => 31
    t.date      "release_date"
    t.decimal   "rating",                     :precision => 4, :scale => 2
    t.string    "thumb"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  add_index "episode", ["created_at"], :name => "episode_created_at_index"
  add_index "episode", ["id"], :name => "episode_id_impressions_count_index"
  add_index "episode", ["imdb_id"], :name => "imdb_id_UNIQUE", :unique => true
  add_index "episode", ["name"], :name => "fulltext_episode"
  add_index "episode", ["tv_show_id", "season", "number"], :name => "season_number_tv_show_id_idx", :unique => true

  create_table "episode_release", :force => true do |t|
    t.integer "episode_id", :limit => 8, :null => false
    t.integer "release_id", :limit => 8, :null => false
  end

  add_index "episode_release", ["episode_id", "release_id"], :name => "UNIQUE_episode_release", :unique => true
  add_index "episode_release", ["episode_id"], :name => "FKD58E61C34BC0EB9F"
  add_index "episode_release", ["release_id"], :name => "FKD58E61C3CDFB4D54"

  create_table "impression", :force => true do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "impression", ["controller_name", "action_name", "ip_address"], :name => "controlleraction_ip_index"
  add_index "impression", ["controller_name", "action_name", "request_hash"], :name => "controlleraction_request_index"
  add_index "impression", ["controller_name", "action_name", "session_hash"], :name => "controlleraction_session_index"
  add_index "impression", ["impressionable_type", "impressionable_id", "ip_address"], :name => "poly_ip_index"
  add_index "impression", ["impressionable_type", "impressionable_id", "request_hash"], :name => "poly_request_index"
  add_index "impression", ["impressionable_type", "impressionable_id", "session_hash"], :name => "poly_session_index"
  add_index "impression", ["impressionable_type", "message", "impressionable_id"], :name => "impressionable_type_message_index", :length => {"impressionable_type"=>nil, "message"=>255, "impressionable_id"=>nil}
  add_index "impression", ["user_id"], :name => "index_impression_on_user_id"

  create_table "movie", :force => true do |t|
    t.string    "name",                                                        :null => false
    t.string    "original_name"
    t.string    "slug"
    t.text      "description"
    t.string    "genre"
    t.integer   "year",            :limit => 2
    t.date      "release_date"
    t.string    "tagline"
    t.string    "language",        :limit => 2
    t.string    "content_rating",  :limit => 31
    t.decimal   "rating",                        :precision => 4, :scale => 2
    t.string    "movie_type",      :limit => 45
    t.string    "status",          :limit => 45
    t.string    "website"
    t.integer   "runtime"
    t.string    "imdb_id",         :limit => 31
    t.string    "themoviedb_id",   :limit => 31
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.timestamp "data_fetched_at"
  end

  add_index "movie", ["id"], :name => "movie_id_impressions_count_index"
  add_index "movie", ["imdb_id"], :name => "imdb_id_UNIQUE", :unique => true
  add_index "movie", ["name", "original_name"], :name => "fulltext_movie"
  add_index "movie", ["slug"], :name => "slug_UNIQUE", :unique => true
  add_index "movie", ["themoviedb_id"], :name => "themoviedb_id_UNIQUE", :unique => true
  add_index "movie", ["year", "name"], :name => "year", :unique => true

  create_table "movie_media", :force => true do |t|
    t.integer   "movie_id",   :limit => 8,   :null => false
    t.string    "media_type", :limit => 127, :null => false
    t.string    "language",   :limit => 2
    t.string    "url",                       :null => false
    t.binary    "url_hash",   :limit => 16,  :null => false
    t.timestamp "created_at",                :null => false
  end

  add_index "movie_media", ["language"], :name => "idx_language"
  add_index "movie_media", ["movie_id", "media_type", "url_hash"], :name => "unique", :unique => true
  add_index "movie_media", ["movie_id"], :name => "fk_movie_media_movie"

  create_table "movie_release", :force => true do |t|
    t.integer "movie_id",   :limit => 8, :null => false
    t.integer "release_id", :limit => 8, :null => false
  end

  add_index "movie_release", ["movie_id", "release_id"], :name => "UNIQUE_movie_release", :unique => true
  add_index "movie_release", ["movie_id"], :name => "FK3EADDF87AC7BD09"
  add_index "movie_release", ["release_id"], :name => "FK3EADDF8CDFB4D54"

  create_table "movie_series", :force => true do |t|
    t.integer "series_id", :limit => 8, :null => false
    t.integer "movie_id",  :limit => 8, :null => false
  end

  add_index "movie_series", ["movie_id"], :name => "FK3361AB683DBD8734"
  add_index "movie_series", ["series_id", "movie_id"], :name => "UNIQUE_series_movie", :unique => true
  add_index "movie_series", ["series_id"], :name => "FK3361AB6847573A0C"

  create_table "release", :force => true do |t|
    t.string "name", :null => false
    t.string "slug", :null => false
  end

  create_table "series", :force => true do |t|
    t.string "name", :null => false
  end

  add_index "series", ["name"], :name => "name", :unique => true

  create_table "settings", :force => true do |t|
    t.string   "var",                      :null => false
    t.text     "value"
    t.integer  "thing_id"
    t.string   "thing_type", :limit => 30
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "settings", ["thing_type", "thing_id", "var"], :name => "index_settings_on_thing_type_and_thing_id_and_var", :unique => true

  create_table "subtitle", :force => true do |t|
    t.integer   "release_id",  :limit => 8,          :null => false
    t.integer   "team_id",     :limit => 8
    t.string    "language",    :limit => 5,          :null => false
    t.text      "description"
    t.text      "text",        :limit => 2147483647, :null => false
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  add_index "subtitle", ["id"], :name => "subtitle_id_impressions_count_index"
  add_index "subtitle", ["release_id", "language"], :name => "unique_release_language_subtitle", :unique => true
  add_index "subtitle", ["release_id"], :name => "FK852F4C18CDFB4D54"
  add_index "subtitle", ["team_id"], :name => "FK852F4C18B4AA7DE0"

  create_table "team", :force => true do |t|
    t.string "name", :null => false
  end

  add_index "team", ["name"], :name => "name", :unique => true

  create_table "tv_show", :force => true do |t|
    t.string    "name",                                                         :null => false
    t.string    "original_name"
    t.integer   "year",             :limit => 2
    t.date      "release_date"
    t.string    "slug"
    t.text      "description"
    t.string    "genre"
    t.string    "network"
    t.string    "airs_day_of_week", :limit => 31
    t.string    "airs_time",        :limit => 31
    t.string    "content_rating",   :limit => 31
    t.decimal   "rating",                         :precision => 4, :scale => 2
    t.integer   "runtime"
    t.string    "status",           :limit => 31
    t.string    "imdb_id",          :limit => 31
    t.string    "thetvdb_id",       :limit => 31
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.timestamp "data_fetched_at"
    t.string    "latest_episode"
  end

  add_index "tv_show", ["id"], :name => "tv_show_id_impressions_count_index"
  add_index "tv_show", ["imdb_id"], :name => "imdb_id_UNIQUE", :unique => true
  add_index "tv_show", ["name", "original_name"], :name => "fulltext_tv_show"
  add_index "tv_show", ["slug"], :name => "slug_UNIQUE", :unique => true
  add_index "tv_show", ["thetvdb_id"], :name => "thetvdb_id_UNIQUE", :unique => true
  add_index "tv_show", ["year", "name"], :name => "year", :unique => true

  create_table "tv_show_media", :force => true do |t|
    t.integer   "tv_show_id", :limit => 8,   :null => false
    t.string    "media_type", :limit => 127, :null => false
    t.integer   "season"
    t.string    "url",                       :null => false
    t.binary    "url_hash",   :limit => 16,  :null => false
    t.string    "language",   :limit => 2
    t.timestamp "created_at",                :null => false
  end

  add_index "tv_show_media", ["tv_show_id"], :name => "fk_tv_show_tv_show_media"
  add_index "tv_show_media", ["url_hash"], :name => "unique", :unique => true

end
