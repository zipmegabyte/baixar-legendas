class AddLatestEpisodeToTvShow < ActiveRecord::Migration
  def change
    add_column :tv_show, :latest_episode, :string
  end
end
