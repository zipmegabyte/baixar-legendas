# Load the rails application
require File.expand_path('../application', __FILE__)
ActiveRecord::Base.pluralize_table_names = false

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

# Initialize the rails application
Baixarlegenda::Application.initialize!
