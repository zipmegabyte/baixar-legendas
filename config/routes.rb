Baixarlegenda::Application.routes.draw do

	# Index
	get '/' => 'application#index'

	# TV Shows
	get '/serie/:tv_show_slug/:season:number(:slug)' => 'episodes#show', :season => /S\d+/i, :number => /E\d+/i, :as => :episode
	#get '/serie/:tv_show_slug/:season_number' => 'seasons#show', :season_number => /S\d{2}/i
	get '/serie/:tv_show_slug' => 'tv_shows#show', :as => :tv_show
	get '/serie/:tv_show_slug/episodios' => 'tv_shows#episodes'
	get '/series(/:page)' => 'tv_shows#all', :page => /\d+/, :as => :tv_shows

	# Movies
	get '/filmes(/:page)' => 'movies#all', :page => /\d+/

	get '/filmes' => 'movies#index', :as => :movies
	get '/filme/:slug' => 'movies#show', :as => :movie

	# Fetch subtitles
	get '/direct/buscar-legendas/filme/:id' => 'movies#fetch_subtitles'
	get '/direct/buscar-legendas/episodio/:id' => 'episodes#fetch_subtitles'

	# Download Subtitles
	scope :format => true, :constraints => { :format => 'srt' } do

		get '/serie/:tv_show_slug/:season:number:slug/:release' => 'subtitles#download', :season => /S\d+/i, :number => /E\d+/i, :release => /[^\/]+/, :slug => /[^\/]*/
		get '/filme/:movie_slug/:release' => 'subtitles#download', :release => /[^\/]+/

	end
	get '/direct/legenda/ping/:id' => 'subtitles#ping', :id => /\d+/

	# Releases
	get '/filme/:movie_slug/:release' => 'releases#show', :release => /[^\/]+/
	get '/serie/:tv_show_slug/:season:number:slug/:release' => 'releases#show', :season => /S\d+/i, :number => /E\d+/i, :release => /[^\/]+/, :slug => /[^\/]*/

	# Images
	get '/media/filme/:dimension/:name' => 'movie_medias#show', :as => :movie_media, :dimension => /(160x240|215|770x370|original)/
	get '/media/serie/:dimension/:name' => 'tv_show_medias#show', :as => :tv_show_media, :dimension => /(160x240|215|770x370|338x190|original)/
	get '/media/episodio/:dimension/:tv_show_slug:season:number' => 'episodes#thumb', :tv_show_slug => /.+_/,  :season => /S\d+/i, :number => /E\d+/i, :dimension => /(338x190|770x370|original)/

	# Application
	get '/upload' => 'subtitles#upload'
	post '/upload' => 'subtitles#upload'

	post '/async_upload' => 'subtitles#async_upload'
	post '/async_upload_auto' => 'subtitles#async_upload', :defaults => { :auto => true }

	get '/search' => 'application#search'
	get '/external_search' => 'application#external_search'

	# Auth
	get '/login' => 'sessions#facebook_login'
	get '/logout' => 'sessions#logout'

	# typos
	get '/series/:tv_show_slug/:season_number:episode_number' => redirect('/serie/%{tv_show_slug}/%{season_number}%{episode_number}')
	get '/series/:tv_show_slug/:season_number' => redirect('/serie/%{tv_show_slug}/%{season_number}')
	get '/series/:tv_show_slug' => redirect('/serie/%{tv_show_slug}'), :tv_show_slug => /\S+/
	get '/serie' => redirect('/series')
	get '/serie/todas' => redirect('/series')
	get '/serie/todos' => redirect('/series')
	get '/series/todas' => redirect('/series')
	get '/series/todos' => redirect('/series')
	get '/filme' => redirect('/filmes')
	get '/filme/todos' => redirect('/filmes')
	get '/filme/todas' => redirect('/filmes')
	get '/filmes/todos' => redirect('/filmes')
	get '/filmes/todas' => redirect('/filmes')
	get '/filmes/:id' => redirect('/filme/%{id}')

	# short urls
	get '/:type:id' => 'application#short_url', :type => /[emtrs]/, :id => /[a-h\d]+/i

	# error templates
	get '/:error' => 'application#error', :error => /(404|422|500|503)/

	#maintenance
	get '/maintenance' => 'application#maintenance'

end
