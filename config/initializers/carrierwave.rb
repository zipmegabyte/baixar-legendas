CarrierWave.configure do |config|
	config.cache_dir = "#{Rails.root}/tmp/"
	config.sftp_host = "#{Rails.application.config.file_server}"
	config.sftp_user = "#{Rails.application.config.file_server_user}"
	config.sftp_folder = "#{Rails.application.config.file_server_root}"
	config.sftp_url = "http://#{Rails.application.config.file_server}"
	config.sftp_options = {
		:password => "#{Rails.application.config.file_server_password}",
		:port     => "#{Rails.application.config.file_server_port}"
	}
	config.storage = :sftp
end