# Set the host name for URL creation
default_host = 'http://www.baixarlegendas.com.br'
SitemapGenerator::Sitemap.default_host = default_host
SitemapGenerator::Sitemap.sitemaps_host = 'http://legendas.de'
SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new

SitemapGenerator::Sitemap.create do	
	# Add a static page
	add '/upload'
	add '/filmes', :changefreq => 'hourly'
	add '/series', :changefreq => 'hourly'
	
	limit = 500
	
	#Movies
	step = 0
	loop do
	
		results = ActiveRecord::Base.connection.select_all( "SELECT slug FROM movie LIMIT #{limit} OFFSET #{limit * step}" )
		
		results.each do |result|
			add "/filme/#{result['slug']}", :changefreq => 'hourly'
		end

		step += 1
		
		break if results.empty?
		
	end
	
	#Movie Releases
	step = 0
	loop do
	
		results = ActiveRecord::Base.connection.select_all( "
			SELECT movie.slug, release.name FROM movie
			INNER JOIN `release` ON release.owner_id = movie.id AND release.owner_type = 'Movie'
			LIMIT #{limit} OFFSET #{limit * step}" )
			
		results.each do |result|
			add "/filme/#{result['slug']}/#{result['name']}", :changefreq => 'weekly'
		end

		step += 1
		
		break if results.empty?
	
	end
	
	#Tv Shows
	step = 0
	loop do
	
		results = ActiveRecord::Base.connection.select_all( "SELECT slug FROM tv_show LIMIT #{limit} OFFSET #{limit * step}" )
		
		results.each do |result|
			add "/serie/#{result['slug']}", :changefreq => 'hourly'
		end

		step += 1
		
		break if results.empty?
	
	end
	
	#Episodes
	step = 0
	loop do
	
		results = ActiveRecord::Base.connection.select_all( "SELECT slug, season, MAX(number) AS number FROM episode INNER JOIN tv_show ON tv_show.id = tv_show_id GROUP BY tv_show_id, season LIMIT #{limit} OFFSET #{limit * step}" )
		
		results.each do |result|
		
			( 1 .. result['number'] ).each do |number|
			
				add "/serie/#{result['slug']}/S#{result['season'].to_s.rjust( 2, '0' )}E#{number.to_s.rjust( 2, '0' )}", :changefreq => 'daily'
				
			end
		end

		step += 1
		
		break if results.empty?
		
	end
	
	#Episode Releases
	step = 0
	loop do
	
		results = ActiveRecord::Base.connection.select_all( "
			SELECT tv_show.slug, episode.season, episode.number, release.name
			FROM tv_show 
			INNER JOIN episode ON tv_show_id = tv_show.id 
			INNER JOIN `release` ON `release`.owner_id = episode.id AND `release`.owner_type = 'Episode'
			LIMIT #{limit} OFFSET #{limit * step}" )
			
		results.each do |result|
			add "/serie/#{result['slug']}/S#{result['season'].to_s.rjust( 2, '0' )}E#{result['number'].to_s.rjust( 2, '0' )}/#{result['name']}", :changefreq => 'weekly'
		end

		step += 1
		
		break if results.empty?
	
	end
	
end