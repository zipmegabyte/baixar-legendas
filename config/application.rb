require File.expand_path('../boot', __FILE__)

require 'rails/all'
require "sprockets/railtie"

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  #Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  Bundler.require(:default, Rails.env)
end

module Baixarlegenda
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    # config.autoload_paths += %W(#{config.root}/extras)
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += Dir["#{config.root}/lib/**/"]

    config.less.paths << File.join(Rails.root, 'app', 'assets', 'stylesheets', 'baixarlegendas-theme')

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    config.time_zome = 'Brasilia'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = 'pt-BR'

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    # config.active_record.schema_format = :sql

    # Enforce whitelist mode for mass assignment.
    # This will create an empty whitelist of attributes available for mass-assignment for all models
    # in your app. As such, your models will need to explicitly whitelist or blacklist accessible
    # parameters by using an attr_accessible or attr_protected declaration.
    # config.active_record.whitelist_attributes = true

    # Enable the asset pipeline
    #config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'
    config.assets.compress = true
    config.assets.js_compressor = :yui
    config.assets.digest = true

    config.thetvdb_api_key = ENV['THETVDB_API_KEY']
    config.themoviedb_api_key = ENV['THEMOVIEDB_API_KEY']
    config.trakt_api_key = ENV['TRAKT_API_KEY']
    config.rotten_tomatoes_api_key = ENV['ROTTEN_TOMATOES_API_KEY']
    config.facebook_app_id = ENV['FACEBOOK_APP_ID']
    config.cloudflare_api_key = ENV['CLOUDFLARE_API_KEY']
    config.cloudflare_email = ENV['CLOUDFLARE_EMAIL']
    config.cloudflare_domain = ENV['CLOUDFLARE_DOMAIN']

    # Enable or disable external data sources
    config.use_thetvdb = true
    config.use_themoviedb = true
    config.use_trakt = true
    config.use_rotten_tomatoes = true

    # In seconds
    config.external_data_fetch_limit = ENV['EXTERNAL_DATA_FETCH_LIMIT'].to_i

    # Regular expression for TV Show
    config.regexp_tv_show = /^(?:.+\\)?((.+?)(?:[(\[]?((?:18|19|20)\d\d)[)\]]?)?[.\s_-](?:(?:s(\d\d)e(\d\d))|(?:(\d?\d)x(\d?\d))|(?:(\d)(\d\d)))[.\s_-]((?:108|72|48)0[pi])?.+)\.\w{3}$/i

    # Regular expression for Movie
    config.regexp_movie = /^((.+?)[(\[]?((?:18|19|20)\d\d)[)\]]?[.\s_-]?((?:108|72|48)0[pi])?.+)\.\w{3}$/i

    # File server URL
    config.file_server = ENV['FILE_SERVER']
    config.file_server_user = ENV['FILE_SERVER_USER']
    config.file_server_root = ENV['FILE_SERVER_ROOT']
    config.file_server_password = ENV['FILE_SERVER_PASSWORD']
    config.file_server_port = ENV['FILE_SERVER_PORT']
    config.image_server = ENV['IMAGE_SERVER']
    #config.image_server = 'localhost'

    config.domain = 'www.baixarlegendas.com.br'

    routes.default_url_options[:only_path] = false

    config.show_featured_movies = 12
    config.show_featured_tv_shows = 12
    config.show_latest_movies = 20
    config.show_latest_tv_shows = 20

    #for heroku
    config.logger = Logger.new(STDOUT)
    config.logger.level = Logger.const_get((ENV['LOG_LEVEL'] || 'INFO').upcase)
    config.assets.initialize_on_precompile = false

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
       address:              ENV['MAIL_ADDRESS'],
       port:                 ENV['MAIL_PORT'],
       domain:               ENV['MAIL_DOMAIN'],
       user_name:            ENV['MAIL_USER_NAME'],
       password:             ENV['MAIL_PASSWORD'],
       authentication:       ENV['MAIL_AUTHENTICATION'],
       enable_starttls_auto: true
    }

    config.home_backgrounds = ENV['HOME_BACKGROUNDS']
  end
end
