set :application, "baixar-legendas"
set :repository,  "https://zipmegabyte@bitbucket.org/zipmegabyte/baixar-legendas.git"
set :deploy_to, "/home/bitnami/#{application}"

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "bitnami"                          # Your HTTP server, Apache/etc
role :app, "bitnami"                          # This may be the same as your `Web` server
role :db,  "bitnami", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

set :user, "bitnami"
set :scm_username, "zipmegabyte"

set :use_sudo, false

set :scm_command, '/opt/bitnami/git/bin/git'
set :local_scm_command, :default

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end